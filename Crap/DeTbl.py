#!/usr/bin/var python2

# This works. Fast and good, too. It is here in the Crap directory
# only because we have to hardcode some things:
# - separator     (line 37)
# - function name (lines 17-18)
# - var name      (lines 17-18)

import sys, struct, binascii
import Tokeniser, DeTokeniser, AU3Utils

def DeTbl(TokensList, TblList):
    Index = 0
    NewTokensList = []
    while Index < len(TokensList):
        Token = TokensList[Index]
        # A0700001D02($CW[1])
        if Token[0] == 0x34 and Token[2] == 'A0700001D02' and TokensList[Index + 1][0] == 0x47 and TokensList[Index + 2][0] == 0x33 and TokensList[Index + 2][2] == 'CW' and TokensList[Index + 3][0] == 0x4E and TokensList[Index + 4][0] == 0x05 and TokensList[Index + 5][0] == 0x4F and TokensList[Index + 6][0] == 0x48:
            Number = TokensList[Index + 4][1]
            String = TblList[Number - 1]
            NewToken = Tokeniser.TextToTokensList('%s\r\n' % String, len(String))[0]
            NewTokensList.append(NewToken)
            Index += 6
        else:
            NewTokensList.append(Token)
        Index += 1
    return NewTokensList

if __name__ == '__main__':
    ScriptText = open(sys.argv[1], 'rb').read() + '\r\n'
    TokensList = Tokeniser.TextToTokensList(ScriptText, len(ScriptText))

    #for Token in TokensList:
    #    print Token

    TBLFile = open(sys.argv[2], 'rb')
    TBLList = TBLFile.read().rstrip().split('i9I')
    TBLFile.close()

    for Elem in xrange(len(TBLList)):
        TBLList[Elem] = AU3Utils.MakeAu3String(binascii.unhexlify(TBLList[Elem]))

    TokensList = DeTbl(TokensList, TBLList)

    TokenData = Tokeniser.TokensListToData(TokensList)
    ScriptText = DeTokeniser.DataToText(TokenData)

    ScriptFile = open(sys.argv[3], 'wb')
    ScriptFile.write(ScriptText)
    ScriptFile.close()
