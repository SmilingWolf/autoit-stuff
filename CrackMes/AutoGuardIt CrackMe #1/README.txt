1) extract ObfuscatedFile.tok using mATE
2) run DeTokeniser.py ObfuscatedFile.tok > asdf.au3
3) run Tidy on asdf.au3
4) run DeObfuscator.py asdf.au3 > asdf2.au3
5) run DeStringEncr.py asdf2.au3 > asdf3.au3
5) run DeCFlow.py asdf3.au3 > asdf4.au3
6) bit of manual work, gotta remove a small part that would make Au3Strip choke
7) run Au3Stripper on asdf4.au3
8) run DeDeadVars.py asdf4_stripped.au3 > asdf5.au3
9) run Tidy on asdf5.au3
