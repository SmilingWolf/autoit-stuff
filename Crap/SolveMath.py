#!/usr/bin/var python2

import sys, struct, binascii
import Tokeniser, DeTokeniser

ScriptText = open(sys.argv[1], 'rb').read() + '\r\n'
ScriptLines, TokensList = Tokeniser.TextToTokensList(ScriptText, len(ScriptText))

# for Elem in TokensList:
#     print Elem

Index = 0
while Index < len(TokensList):
    NumLeft = None
    NumRight = None
    Sign = None
#     print TokensList[Index][0]
    if TokensList[Index][0] == 0x05:
        NumLeft = TokensList[Index][1]
        Index += 1
        if TokensList[Index][0] in {0x49, 0x4A}:
            Sign = TokensList[Index][0]
            Index += 1
            if TokensList[Index][0] == 0x05:
                NumRight = TokensList[Index][1]
                print 'Got a math expression!'
    if None not in {Sign, NumLeft, NumRight}:
        if Sign == 0x49:
            NewNum = NumLeft + NumRight
        elif Sign == 0x4A:
            NewNum = NumLeft - NumRight
        TokensList.pop(Index)
        TokensList.pop(Index - 1)
        TokensList.pop(Index - 2)
        TmpList = TokensList[:Index - 2]
        TmpList.append([0x05, NewNum])
        TmpList += TokensList[Index - 2:]
        TokensList = TmpList
        Index -= 2
    Index += 1

# for Elem in TokensList:
#     print Elem

TokenData = Tokeniser.TokensListToData(TokensList)
ScriptText = DeTokeniser.DataToText(struct.pack('<L', ScriptLines) + TokenData)

ScriptFile = open(sys.argv[2], 'wb')
ScriptFile.write(ScriptText)
ScriptFile.close()
