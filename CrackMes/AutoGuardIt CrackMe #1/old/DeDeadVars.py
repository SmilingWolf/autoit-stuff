#!/usr/bin/env python2

import sys, re

Lines = []
Script = open(sys.argv[1], 'r')
CurrLine = Script.readline()
while CurrLine != '':
    Lines.append(CurrLine.lstrip('\t').rstrip('\r\n'))
    CurrLine = Script.readline()

NewScript = []
SearchBuffer = ''.join(Lines)
for Elem in Lines:
    if Elem.startswith('$'):
        Variable = Elem.split('=')[0].lstrip(' ').rstrip(' ')
        Value = Elem.split('=')[1].lstrip(' ').rstrip(' ')
        # print Value
        # print re.match('\w(.*)', Value)
        if re.match('[a-zA-Z](.*)', Value) == None:
            InsVar = re.compile(re.escape(Variable), re.IGNORECASE)
            if len(InsVar.findall(SearchBuffer)) > 1:
                NewScript.append(Elem)
        else:
            NewScript.append(Elem)
    else:
        NewScript.append(Elem)

for Elem in NewScript:
    print Elem
