#!/usr/bin/var python2

import sys, struct, binascii
import Tokeniser, DeTokeniser, AU3Utils

class AU3Variable:
    def __init__(self, Name, StartOffset, Length, Used):
        self.Name = Name
        self.StartOffset = StartOffset
        self.Length = Length
        self.Used = Used

def RemoveDeadFuncs(TokensList):
    # Traverse the list to find all the Func statements.
    # Create an instance of an AU3Variable class with the Name,
    # Index in the TokensList, length of the function from FUNC to ENDFUNC + newline
    # and add it to the FuncsList
    Index = 0
    FuncsList = []
    while Index < len(TokensList):
        if TokensList[Index][0] == 0x30 and TokensList[Index][2] == 'FUNC':
            Name = TokensList[Index + 1][2]
            StartOffset = Index
            Index += 1
            while 1:
                if TokensList[Index][0] == 0x30 and TokensList[Index][2] == 'ENDFUNC':
                    break
                else:
                    Index += 1
            Length = (Index - StartOffset) + 2
            FuncsList.append(AU3Variable(Name, StartOffset, Length, False))
        Index += 1

    # For every Function found traverse TokensList. When we get to the beginning of the
    # function declaration just skip all of it. When we find a call to a user-defined
    # function check if the name is that of the current one. If it is it means the function
    # is being called, so mark it as used
    for Function in FuncsList:
        Index = 0
        while Index < len(TokensList):
            if Index == Function.StartOffset:
                Index += (Function.Length - 1)
            if TokensList[Index][0] == 0x34 and Function.Name == TokensList[Index][2]:
                # print 'Setting %s as used' % Function.Name
                Function.Used = True
                break
            Index += 1

    # For every Function check if it has been set as used. If it has not been,
    # then we can somewhat safely remove it (there are some edge cases such as
    # Call("YoFunction") that are not currently being handled).
    # Pop Function.Length tokens from the TokensList.
    # Of course this will move back any other token into the list.
    # Take this displacement into account using the Offset variable, to which
    # every time we add the number of removed tokens
    Offset = 0
    for Function in FuncsList:
        if Function.Used == False:
            for NumPops in xrange(Function.Length):
                TokensList.pop(Function.StartOffset - Offset)
            Offset += Function.Length
    return TokensList

def RemoveDeadVars(TokensList):
    # Traverse the list to find all the variables at the beginning of the line
    # We are keeping it simple right now, not considering Global/Local/Dim prefixed ones
    # Also check if we are dealing with a plain ('=') assignment and if yes
    # if what's being assigned is not the return value of a function (built-in or user defined).
    # If all the requirements are fulfilled create an instance of an AU3Variable class
    # with the Name, Index in the TokensList, length of the var (from name to end of line)
    # and add it to the VarsList
    Index = 0
    VarsList = []
    while Index < len(TokensList):
        if TokensList[Index][0] == 0x33 and (Index == 0 or TokensList[Index - 1][0] == 0x7F):
            if TokensList[Index + 1][0] == 0x41 and TokensList[Index + 2][0] not in {0x31, 0x34}:
                Name = TokensList[Index][2]
                StartOffset = Index
                Index += 1
                while TokensList[Index][0] not in {0x40, 0x7F}:
                    Index += 1
                Length = (Index - StartOffset) + 1
                VarsList.append(AU3Variable(Name, StartOffset, Length, False))
        Index += 1

    # For every Var found traverse TokensList. Every time we meet a variable there
    # check if it is at the beginning of a line and if it is check if the name of this
    # variable is equal to the one we are examining from the VarsList.
    # If it is not at the beginning of a line mark it as used
    for Variable in VarsList:
        Index = 0
        while Index < len(TokensList):
            if TokensList[Index][0] == 0x33 and Index != 0 and TokensList[Index - 1][0] != 0x7F and Variable.Name == TokensList[Index][2]:
                # print 'Setting %s as used' % Variable.Name
                Variable.Used = True
                break
            Index += 1

    # For every Variable check if it has been set as used. If it has not been,
    # then we can somewhat safely remove it. Pop Variable.Length tokens from the
    # TokensList. Of course this will move back any other token into the list.
    # Take this displacement into account using the Offset variable, to which
    # every time we add the number of removed tokens
    Offset = 0
    for Variable in VarsList:
        if Variable.Used == False:
            for NumPops in xrange(Variable.Length):
                TokensList.pop(Variable.StartOffset - Offset)
            Offset += Variable.Length
    return TokensList

if __name__ == '__main__':
    ScriptText = open(sys.argv[1], 'rb').read() + '\r\n'
    TokensList = Tokeniser.TextToTokensList(ScriptText, len(ScriptText))

    # for Elem in TokensList:
    #     print Elem

    while 1:
        NumLines = AU3Utils.CountLines(TokensList)
        NumTokens = len(TokensList)
        TokensList = RemoveDeadFuncs(TokensList)
        LinesAfterDF = AU3Utils.CountLines(TokensList)
        print 'DeadFunctions: Removed %s lines' % (NumLines - LinesAfterDF)
        TokensList = RemoveDeadVars(TokensList)
        LinesAfterDV = AU3Utils.CountLines(TokensList)
        print 'DeadVariables: Removed %s lines' % (LinesAfterDF - LinesAfterDV)
        if NumTokens == len(TokensList):
            break

    # for Elem in TokensList:
    #     print Elem

    TokenData = Tokeniser.TokensListToData(TokensList)
    ScriptText = DeTokeniser.DataToText(TokenData)

    ScriptFile = open(sys.argv[2], 'wb')
    ScriptFile.write(ScriptText)
    ScriptFile.close()
