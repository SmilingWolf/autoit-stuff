#!/usr/bin/var python2

import sys, struct
from AU3Utils import EncDecString, CountLines

Base10Alpha = ['.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
Base16Alpha = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'a', 'b', 'c', 'd', 'e', 'f']
KeywordsList = ['AND', 'OR', 'NOT', 'IF', 'THEN', 'ELSE', 'ELSEIF', 'ENDIF', 'WHILE', 'WEND', 'DO', 'UNTIL', 'FOR', 'NEXT', 'TO', 'STEP', 'IN', 'EXITLOOP', 'CONTINUELOOP', 'SELECT', 'CASE', 'ENDSELECT', 'SWITCH', 'ENDSWITCH', 'CONTINUECASE', 'DIM', 'REDIM', 'LOCAL', 'GLOBAL', 'CONST', 'STATIC', 'FUNC', 'ENDFUNC', 'RETURN', 'EXIT', 'BYREF', 'WITH', 'ENDWITH', 'TRUE', 'FALSE', 'DEFAULT', 'NULL']
BuiltinsList = ['ACOS', 'ADLIBREGISTER', 'ADLIBUNREGISTER', 'ASC', 'ASCW', 'ASIN', 'ASSIGN', 'ATAN', 'AUTOITSETOPTION', 'AUTOITWINGETTITLE', 'AUTOITWINSETTITLE', 'BEEP', 'BINARY', 'BINARYLEN', 'BINARYMID', 'BINARYTOSTRING', 'BITAND', 'BITNOT', 'BITOR', 'BITROTATE', 'BITSHIFT', 'BITXOR', 'BLOCKINPUT', 'BREAK', 'CALL', 'CDTRAY', 'CEILING', 'CHR', 'CHRW', 'CLIPGET', 'CLIPPUT', 'CONSOLEREAD', 'CONSOLEWRITE', 'CONSOLEWRITEERROR', 'CONTROLCLICK', 'CONTROLCOMMAND', 'CONTROLDISABLE', 'CONTROLENABLE', 'CONTROLFOCUS', 'CONTROLGETFOCUS', 'CONTROLGETHANDLE', 'CONTROLGETPOS', 'CONTROLGETTEXT', 'CONTROLHIDE', 'CONTROLLISTVIEW', 'CONTROLMOVE', 'CONTROLSEND', 'CONTROLSETTEXT', 'CONTROLSHOW', 'CONTROLTREEVIEW', 'COS', 'DEC', 'DIRCOPY', 'DIRCREATE', 'DIRGETSIZE', 'DIRMOVE', 'DIRREMOVE', 'DLLCALL', 'DLLCALLADDRESS', 'DLLCALLBACKFREE', 'DLLCALLBACKGETPTR', 'DLLCALLBACKREGISTER', 'DLLCLOSE', 'DLLOPEN', 'DLLSTRUCTCREATE', 'DLLSTRUCTGETDATA', 'DLLSTRUCTGETPTR', 'DLLSTRUCTGETSIZE', 'DLLSTRUCTSETDATA', 'DRIVEGETDRIVE', 'DRIVEGETFILESYSTEM', 'DRIVEGETLABEL', 'DRIVEGETSERIAL', 'DRIVEGETTYPE', 'DRIVEMAPADD', 'DRIVEMAPDEL', 'DRIVEMAPGET', 'DRIVESETLABEL', 'DRIVESPACEFREE', 'DRIVESPACETOTAL', 'DRIVESTATUS', 'DUMMYSPEEDTEST', 'ENVGET', 'ENVSET', 'ENVUPDATE', 'EVAL', 'EXECUTE', 'EXP', 'FILECHANGEDIR', 'FILECLOSE', 'FILECOPY', 'FILECREATENTFSLINK', 'FILECREATESHORTCUT', 'FILEDELETE', 'FILEEXISTS', 'FILEFINDFIRSTFILE', 'FILEFINDNEXTFILE', 'FILEFLUSH', 'FILEGETATTRIB', 'FILEGETENCODING', 'FILEGETLONGNAME', 'FILEGETPOS', 'FILEGETSHORTCUT', 'FILEGETSHORTNAME', 'FILEGETSIZE', 'FILEGETTIME', 'FILEGETVERSION', 'FILEINSTALL', 'FILEMOVE', 'FILEOPEN', 'FILEOPENDIALOG', 'FILEREAD', 'FILEREADLINE', 'FILEREADTOARRAY', 'FILERECYCLE', 'FILERECYCLEEMPTY', 'FILESAVEDIALOG', 'FILESELECTFOLDER', 'FILESETATTRIB', 'FILESETEND', 'FILESETPOS', 'FILESETTIME', 'FILEWRITE', 'FILEWRITELINE', 'FLOOR', 'FTPSETPROXY', 'FUNCNAME', 'GUICREATE', 'GUICTRLCREATEAVI', 'GUICTRLCREATEBUTTON', 'GUICTRLCREATECHECKBOX', 'GUICTRLCREATECOMBO', 'GUICTRLCREATECONTEXTMENU', 'GUICTRLCREATEDATE', 'GUICTRLCREATEDUMMY', 'GUICTRLCREATEEDIT', 'GUICTRLCREATEGRAPHIC', 'GUICTRLCREATEGROUP', 'GUICTRLCREATEICON', 'GUICTRLCREATEINPUT', 'GUICTRLCREATELABEL', 'GUICTRLCREATELIST', 'GUICTRLCREATELISTVIEW', 'GUICTRLCREATELISTVIEWITEM', 'GUICTRLCREATEMENU', 'GUICTRLCREATEMENUITEM', 'GUICTRLCREATEMONTHCAL', 'GUICTRLCREATEOBJ', 'GUICTRLCREATEPIC', 'GUICTRLCREATEPROGRESS', 'GUICTRLCREATERADIO', 'GUICTRLCREATESLIDER', 'GUICTRLCREATETAB', 'GUICTRLCREATETABITEM', 'GUICTRLCREATETREEVIEW', 'GUICTRLCREATETREEVIEWITEM', 'GUICTRLCREATEUPDOWN', 'GUICTRLDELETE', 'GUICTRLGETHANDLE', 'GUICTRLGETSTATE', 'GUICTRLREAD', 'GUICTRLRECVMSG', 'GUICTRLREGISTERLISTVIEWSORT', 'GUICTRLSENDMSG', 'GUICTRLSENDTODUMMY', 'GUICTRLSETBKCOLOR', 'GUICTRLSETCOLOR', 'GUICTRLSETCURSOR', 'GUICTRLSETDATA', 'GUICTRLSETDEFBKCOLOR', 'GUICTRLSETDEFCOLOR', 'GUICTRLSETFONT', 'GUICTRLSETGRAPHIC', 'GUICTRLSETIMAGE', 'GUICTRLSETLIMIT', 'GUICTRLSETONEVENT', 'GUICTRLSETPOS', 'GUICTRLSETRESIZING', 'GUICTRLSETSTATE', 'GUICTRLSETSTYLE', 'GUICTRLSETTIP', 'GUIDELETE', 'GUIGETCURSORINFO', 'GUIGETMSG', 'GUIGETSTYLE', 'GUIREGISTERMSG', 'GUISETACCELERATORS', 'GUISETBKCOLOR', 'GUISETCOORD', 'GUISETCURSOR', 'GUISETFONT', 'GUISETHELP', 'GUISETICON', 'GUISETONEVENT', 'GUISETSTATE', 'GUISETSTYLE', 'GUISTARTGROUP', 'GUISWITCH', 'HEX', 'HOTKEYSET', 'HTTPSETPROXY', 'HTTPSETUSERAGENT', 'HWND', 'INETCLOSE', 'INETGET', 'INETGETINFO', 'INETGETSIZE', 'INETREAD', 'INIDELETE', 'INIREAD', 'INIREADSECTION', 'INIREADSECTIONNAMES', 'INIRENAMESECTION', 'INIWRITE', 'INIWRITESECTION', 'INPUTBOX', 'INT', 'ISADMIN', 'ISARRAY', 'ISBINARY', 'ISBOOL', 'ISDECLARED', 'ISDLLSTRUCT', 'ISFLOAT', 'ISFUNC', 'ISHWND', 'ISINT', 'ISKEYWORD', 'ISMAP', 'ISNUMBER', 'ISOBJ', 'ISPTR', 'ISSTRING', 'LOG', 'MAPAPPEND', 'MAPEXISTS', 'MAPKEYS', 'MAPREMOVE', 'MEMGETSTATS', 'MOD', 'MOUSECLICK', 'MOUSECLICKDRAG', 'MOUSEDOWN', 'MOUSEGETCURSOR', 'MOUSEGETPOS', 'MOUSEMOVE', 'MOUSEUP', 'MOUSEWHEEL', 'MSGBOX', 'NUMBER', 'OBJCREATE', 'OBJCREATEINTERFACE', 'OBJEVENT', 'OBJGET', 'OBJNAME', 'ONAUTOITEXITREGISTER', 'ONAUTOITEXITUNREGISTER', 'OPT', 'PING', 'PIXELCHECKSUM', 'PIXELGETCOLOR', 'PIXELSEARCH', 'PROCESSCLOSE', 'PROCESSEXISTS', 'PROCESSGETSTATS', 'PROCESSLIST', 'PROCESSSETPRIORITY', 'PROCESSWAIT', 'PROCESSWAITCLOSE', 'PROGRESSOFF', 'PROGRESSON', 'PROGRESSSET', 'PTR', 'RANDOM', 'REGDELETE', 'REGENUMKEY', 'REGENUMVAL', 'REGREAD', 'REGWRITE', 'ROUND', 'RUN', 'RUNAS', 'RUNASWAIT', 'RUNWAIT', 'SEND', 'SENDKEEPACTIVE', 'SETERROR', 'SETEXTENDED', 'SHELLEXECUTE', 'SHELLEXECUTEWAIT', 'SHUTDOWN', 'SIN', 'SLEEP', 'SOUNDPLAY', 'SOUNDSETWAVEVOLUME', 'SPLASHIMAGEON', 'SPLASHOFF', 'SPLASHTEXTON', 'SQRT', 'SRANDOM', 'STATUSBARGETTEXT', 'STDERRREAD', 'STDINWRITE', 'STDIOCLOSE', 'STDOUTREAD', 'STRING', 'STRINGADDCR', 'STRINGCOMPARE', 'STRINGFORMAT', 'STRINGFROMASCIIARRAY', 'STRINGINSTR', 'STRINGISALNUM', 'STRINGISALPHA', 'STRINGISASCII', 'STRINGISDIGIT', 'STRINGISFLOAT', 'STRINGISINT', 'STRINGISLOWER', 'STRINGISSPACE', 'STRINGISUPPER', 'STRINGISXDIGIT', 'STRINGLEFT', 'STRINGLEN', 'STRINGLOWER', 'STRINGMID', 'STRINGREGEXP', 'STRINGREGEXPREPLACE', 'STRINGREPLACE', 'STRINGREVERSE', 'STRINGRIGHT', 'STRINGSPLIT', 'STRINGSTRIPCR', 'STRINGSTRIPWS', 'STRINGTOASCIIARRAY', 'STRINGTOBINARY', 'STRINGTRIMLEFT', 'STRINGTRIMRIGHT', 'STRINGUPPER', 'TAN', 'TCPACCEPT', 'TCPCLOSESOCKET', 'TCPCONNECT', 'TCPLISTEN', 'TCPNAMETOIP', 'TCPRECV', 'TCPSEND', 'TCPSHUTDOWN', 'TCPSTARTUP', 'TIMERDIFF', 'TIMERINIT', 'TOOLTIP', 'TRAYCREATEITEM', 'TRAYCREATEMENU', 'TRAYGETMSG', 'TRAYITEMDELETE', 'TRAYITEMGETHANDLE', 'TRAYITEMGETSTATE', 'TRAYITEMGETTEXT', 'TRAYITEMSETONEVENT', 'TRAYITEMSETSTATE', 'TRAYITEMSETTEXT', 'TRAYSETCLICK', 'TRAYSETICON', 'TRAYSETONEVENT', 'TRAYSETPAUSEICON', 'TRAYSETSTATE', 'TRAYSETTOOLTIP', 'TRAYTIP', 'UBOUND', 'UDPBIND', 'UDPCLOSESOCKET', 'UDPOPEN', 'UDPRECV', 'UDPSEND', 'UDPSHUTDOWN', 'UDPSTARTUP', 'VARGETTYPE', 'WINACTIVATE', 'WINACTIVE', 'WINCLOSE', 'WINEXISTS', 'WINFLASH', 'WINGETCARETPOS', 'WINGETCLASSLIST', 'WINGETCLIENTSIZE', 'WINGETHANDLE', 'WINGETPOS', 'WINGETPROCESS', 'WINGETSTATE', 'WINGETTEXT', 'WINGETTITLE', 'WINKILL', 'WINLIST', 'WINMENUSELECTITEM', 'WINMINIMIZEALL', 'WINMINIMIZEALLUNDO', 'WINMOVE', 'WINSETONTOP', 'WINSETSTATE', 'WINSETTITLE', 'WINSETTRANS', 'WINWAIT', 'WINWAITACTIVE', 'WINWAITCLOSE', 'WINWAITNOTACTIVE']

def TextToTokensList(InputText, TextLen):
    TokensList = []
    TextCharCounter = 0
    Char = InputText[TextCharCounter]
    while 1:
        TokenDataList = []
        Command = 0
        SubCmd = 0
        Buffer = ''
        while Char == '\t' or Char == ' ':
            TextCharCounter += 1
            Char = InputText[TextCharCounter]
        if (Char >= '0' and Char <= '9'):
            Base = 10
            Alphabet = Base10Alpha
            String = Char
            TextCharCounter += 1
            Char = InputText[TextCharCounter]
            if Char == 'x':
                Base = 16
                Alphabet = Base16Alpha
                TextCharCounter += 1
                Char = InputText[TextCharCounter]
            while Char in Alphabet:
                String += Char
                TextCharCounter += 1
                Char = InputText[TextCharCounter]
            TextCharCounter -= 1
            if '.' not in String:
                SubCmd = int('%s' % String, Base)
            else:
                SubCmd = float('%s' % String)
            if type(SubCmd) != float and TokensList[len(TokensList) - 1][0] == 0x4A and SubCmd <= 0x7FFFFFFF:
                TokensList.pop()
                TokensList.append([0x49])
                SubCmd = int('-%s' % String, Base)
            if type(SubCmd) == float:
                Command = 0x20
            elif abs(SubCmd) <= 0xFFFFFFFF:
                Command = 0x05
            else:
                Command = 0x10
        elif Char == '_' or Char == '$' or Char == '@' or Char == '.' or (Char >= 'A' and Char <= 'Z') or (Char >= 'a' and Char <= 'z'):
            String = Char
            TextCharCounter += 1
            Char = InputText[TextCharCounter]
            while (Char >= '0' and Char <= '9') or (Char >= 'A' and Char <= 'Z') or (Char >= 'a' and Char <= 'z') or Char == '_':
                String += Char
                TextCharCounter += 1
                Char = InputText[TextCharCounter]
            TextCharCounter -= 1
            String = String.upper()
            if String in KeywordsList:
                # Command = 0x00
                # SubCmd = KeywordsList.index(String) + 1
                Command = 0x30
                SubCmd = len(String)
                Buffer = String
            elif String in BuiltinsList:
                # Command = 0x01
                # SubCmd = BuiltinsList.index(String) + 1
                Command = 0x31
                SubCmd = len(String)
                Buffer = String
            elif String.startswith('@'):
                Command = 0x32
                String = String[1:]
                SubCmd = len(String)
                Buffer = String
            elif String.startswith('$'):
                Command = 0x33
                String = String[1:]
                SubCmd = len(String)
                Buffer = String
            elif String.startswith('.'):
                Command = 0x35
                String = String[1:]
                SubCmd = len(String)
                Buffer = String
            else:
                Command = 0x34
                SubCmd = len(String)
                Buffer = String
        elif Char == '#':
            String = Char
            TextCharCounter += 1
            Char = InputText[TextCharCounter]
            while Char not in ['\r', '\n']:
                String += Char
                TextCharCounter += 1
                Char = InputText[TextCharCounter]
            TextCharCounter -= 1
            Command = 0x37
            SubCmd = len(String)
            Buffer = String
        elif Char == '"' or Char == '\'':
            String = ''
            StringDelimiter = Char
            while 1:
                TextCharCounter += 1
                if InputText[TextCharCounter] == StringDelimiter:
                    if InputText[TextCharCounter + 1] == StringDelimiter:
                        Char = InputText[TextCharCounter]
                        String += Char
                        TextCharCounter += 1
                    else:
                        break
                else:
                    Char = InputText[TextCharCounter]
                    String += Char
            Command = 0x36
            SubCmd = len(String.decode('utf-8').encode('utf-16-le')) / 2
            Buffer = String
        elif Char == ',':
            Command = 0x40
        elif Char == '=':
            if InputText[TextCharCounter + 1] == '=':
                Command = 0x50
                TextCharCounter += 1
            else:
                Command = 0x41
        elif Char == '>':
            if InputText[TextCharCounter + 1] == '=':
                Command = 0x45
                TextCharCounter += 1
            else:
                Command = 0x42
        elif Char == '<':
            if InputText[TextCharCounter + 1] == '=':
                Command = 0x46
                TextCharCounter += 1
            elif InputText[TextCharCounter + 1] == '>':
                Command = 0x44
                TextCharCounter += 1
            else:
                Command = 0x43
        elif Char == '(':
            Command = 0x47
        elif Char == ')':
            Command = 0x48
        elif Char == '+':
            if InputText[TextCharCounter + 1] == '=':
                Command = 0x52
                TextCharCounter += 1
            else:
                Command = 0x49
        elif Char == '-':
            if InputText[TextCharCounter + 1] == '=':
                Command = 0x53
                TextCharCounter += 1
            else:
                Command = 0x4A
        elif Char == '/':
            if InputText[TextCharCounter + 1] == '=':
                Command = 0x54
                TextCharCounter += 1
            else:
                Command = 0x4B
        elif Char == '*':
            if InputText[TextCharCounter + 1] == '=':
                Command = 0x55
                TextCharCounter += 1
            else:
                Command = 0x4C
        elif Char == '&':
            if InputText[TextCharCounter + 1] == '=':
                Command = 0x56
                TextCharCounter += 1
            else:
                Command = 0x4D
        elif Char == '[':
            Command = 0x4E
        elif Char == ']':
            Command = 0x4F
        elif Char == '^':
            Command = 0x51
        elif Char == '?':
            Command = 0x57
        elif Char == ':':
            Command = 0x58
        elif Char == '\r' or Char == '\n':
            Command = 0x7F
            TextCharCounter += 1
            Char = InputText[TextCharCounter]
            while Char == '\r' or Char == '\n':
                TextCharCounter += 1
                if TextCharCounter < TextLen:
                    Char = InputText[TextCharCounter]
                else:
                    break
            TextCharCounter -= 1
        TokenDataList.append(Command)
        if Command == 0x05 or Command == 0x20 or Command == 0x36 or SubCmd != 0:
            TokenDataList.append(SubCmd)
        if Buffer != '':
            TokenDataList.append(Buffer)
        TokensList.append(TokenDataList)
        TextCharCounter += 1
        if TextCharCounter < TextLen:
            Char = InputText[TextCharCounter]
        else:
            break
    return TokensList

def TokensListToData(TokensList):
    Data = struct.pack('<L', CountLines(TokensList))
    for Elem in TokensList:
        Data += struct.pack('<b', Elem[0])
        if len(Elem) > 1:
            if type(Elem[1]) == float:
                Data += struct.pack('<d', Elem[1])
            elif Elem[1] <= 0x7FFFFFFF:
                Data += struct.pack('<l', Elem[1])
            elif Elem[1] <= 0xFFFFFFFF:
                Data += struct.pack('<L', Elem[1])
            elif Elem[1] <= 0x7FFFFFFFFFFFFFFF:
                Data += struct.pack('<q', Elem[1])
            elif Elem[1] <= 0xFFFFFFFFFFFFFFFF:
                Data += struct.pack('<Q', Elem[1])
        if len(Elem) > 2:
            Data += EncDecString(Elem[1], Elem[2].decode('utf-8').encode('utf-16-le'))
    return Data

if __name__ == '__main__':
    ScriptText = open(sys.argv[1], 'rb').read() + '\r\n'
    TokensList = TextToTokensList(ScriptText, len(ScriptText))
    TokenData = TokensListToData(TokensList)

    TokFile = open(sys.argv[2], 'wb')
    TokFile.write(TokenData)
    TokFile.close()
