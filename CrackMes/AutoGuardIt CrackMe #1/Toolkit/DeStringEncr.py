#!/usr/bin/env python2

import sys, re, subprocess

ScriptFile = open(sys.argv[1], 'r')

FuncDict = {}
Line = ScriptFile.readline()
while Line != '':
    if Line.startswith('Func '):
        FuncName = Line[5:]
        FuncName = re.sub('\(.*\)', '', FuncName).rstrip('\r\n')
        FuncList = []
        FuncList.append(Line)
        Line = ScriptFile.readline()
        while not Line.startswith('EndFunc'):
            FuncList.append(Line)
            Line = ScriptFile.readline()
        FuncList.append('EndFunc\n')
        FuncDict[FuncName] = FuncList
    else:
        Line = ScriptFile.readline()

FunctionsDoneList = []
DecrScriptList = []
DecrScriptList.append('$hFile = FileOpen(\'FuncNames.txt\', 2)\n')
ScriptFile.seek(0, 0)
Script = ScriptFile.read()
BinToStrRE = re.compile('BinaryToString\(\w{1,}\(\)\)')
FullLines = BinToStrRE.findall(Script)
for Elem in FullLines:
    FuncName = Elem[15:-3]
    if FuncName not in FunctionsDoneList:
        FunctionsDoneList.append(FuncName)
        for FuncLine in FuncDict[FuncName]:
            DecrScriptList.append(FuncLine)
        DecrScriptList.append('$Assignement = \'%s\'\n' % Elem)
        DecrScriptList.append('FileWriteLine($hFile, $Assignement & \' = "\' & %s & \'"\')\n' % Elem)
DecrScriptList.append('FileClose($hFile)')

FuncScript = open('NameDecrypter.au3', 'w')
FuncScript.writelines(DecrScriptList)
FuncScript.close()
subprocess.call('"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" NameDecrypter.au3', shell=True)
DeobfAssign = open('FuncNames.txt', 'r').readlines()
for Elem in DeobfAssign:
    Crypted = Elem.split('=', 1)[0].lstrip(' ').rstrip(' ')
    Decrypted = Elem.split('=', 1)[1].lstrip(' ').rstrip(' ')
    if Decrypted.rstrip('\r\n') == '"\x00\x00\x00\x00"':
        Decrypted = 'Null'
    Script = re.sub(re.escape(Crypted), Decrypted.rstrip('\r\n'), Script)

print Script
