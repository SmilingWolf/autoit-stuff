#!/usr/bin/env python2

# Actually, the logic is pure crap.
# This thing would need to be rewritten
# after thinking better about
# how we should handle all this stuff
# Then again, I have written this hastily
# to have something to attach while posting
# an answer to kao, so as long as it works(TM)...

import sys, re

Lines = []
Script = open(sys.argv[1], 'r')
CurrLine = Script.readline()
while CurrLine != '':
    Lines.append(CurrLine.lstrip('\t').rstrip('\r\n'))
    CurrLine = Script.readline()

CurrLine = 0
VarsDict = {}
NewFlow = []
While1Count = 0
GoodSwitch = 0
ExitLoop = 0
while CurrLine < len(Lines):
    Elem = Lines[CurrLine]
    # print 'Top: %s' % Elem
    NewFlow.append(Elem)
    if Elem.startswith('$'):
        Variable = Elem.split('=')[0].lstrip(' ').rstrip(' ')
        Value = Elem.split('=')[1].lstrip(' ').rstrip(' ')
        VarsDict[Variable] = Value
    elif Elem.startswith('While 1'):
        While1Count += 1
    elif Elem.startswith('Switch ') and While1Count > 0:
        CasesDict = {}
        SwitchVar = Elem[7:]
        if re.match('^\w{1,}\(.*\)', VarsDict[SwitchVar]) == None:
            GoodSwitch = 1
            NewFlow.pop()
        else:
            While1Count -= 1
    elif Elem.startswith('Case ') and While1Count > 0 and GoodSwitch == 1:
        NewFlow.pop()
        CaseVal = Elem[5:]
        CaseContent = []
        while not Lines[CurrLine + 1].startswith('Case ') and not Lines[CurrLine + 1].startswith('EndSwitch'):
            CurrLine += 1
            Elem = Lines[CurrLine]
            CaseContent.append(Elem)
        CasesDict[CaseVal] = CaseContent
    elif Elem.startswith('EndSwitch') and While1Count > 0 and GoodSwitch == 1:
        # for Elem in NewFlow:
        #     print Elem
        NewFlow.pop()
        # print SwitchVar
        # print VarsDict
        # print CasesDict
        SwitchFlow = []
        InLoop = 1
        while InLoop == 1:
            CurrentCase = CasesDict[VarsDict[SwitchVar]]
            for Elem in CurrentCase:
                if Elem.startswith('$') and Elem.split('=')[0].lstrip(' ').rstrip(' ') == SwitchVar:
                    VarsDict[SwitchVar] = Elem.split('=')[1].lstrip(' ').rstrip(' ')
                elif Elem.startswith('ExitLoop'):
                    ExitLoop += 1
                    InLoop = 0
                    break
                else:
                    SwitchFlow.append(Elem)
        # print SwitchFlow
        # print NewFlow
        NewFlow.pop((len(NewFlow) - 1) - NewFlow[::-1].index('While 1'))
        for Elem in SwitchFlow:
            NewFlow.append(Elem)
        GoodSwitch = 0
    elif Elem.startswith('WEnd') and While1Count > 0:
        While1Count -= 1
        if ExitLoop > 0:
            NewFlow.pop()
            # NewFlow.append('ExitLoop')
            # NewFlow.append('Wend')
            ExitLoop -= 1
    CurrLine += 1

for Elem in NewFlow:
    print Elem
