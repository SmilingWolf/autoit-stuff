#RequireAdmin
#NoTrayIcon
Global Const $Var0000 = -3
Global Const $Var0001 = -13
Global Const $Var0002 = 4
Global Const $Var0003 = 8
Global Const $Var0004 = 16
Global Const $Var0005 = 64
Global Const $Var0006 = 128
Global Const $Var0007 = 16
Global Const $Var0008 = 256
Global Const $Var0009 = 64
Global Const $Var000A = 2
Global Const $Var000B = 2
Global Const $Var000C = 4
Func Func0000($Var000D)
	Local $Var000E = StringLen($Var000D)
	If $Var000E < 1 Then Return SetError(1, 0, "")
	Local $Var000F = DllStructCreate("char[" & $Var000E + 1 & "]")
	DllStructSetData($Var000F, 1, $Var000D)
	Local $Var0010 = DllCall("msvcrt.dll", "ptr:cdecl", "_strrev", "struct*", $Var000F)
	If @error Or $Var0010[0] = 0 Then Return SetError(2, 0, "")
	Return DllStructGetData($Var000F, 1)
EndFunc
Func Func0001($Var0011)
	Return Hex(StringToBinary($Var0011))
EndFunc
Func0011("CWAutCompMUTEX")
Global $Var0012, $Var0013, $Var0014 = "2.16", $Var0015 = "v3.3.9.22", $Var0016 = "11/11/2013", $Var0017, $Var0018, $Var0019, $Var001A, $Var001B, $Var001C, $Var001D, $Var001E, $Var001F
$Var0020 = RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\AutoIt v3\AutoIt", "InstallDir")
If @error Then
	MsgBox(16, "O_o", "I need AutoIT installed to work!" & @CRLF & "Or i not have privilege to read Registry?")
	Exit (-1)
EndIf
DirCreate(@TempDir & "\CWAutComp")
Global $Var0021 = "CWAutComp v" & $Var0014
$Var001E = GUICreate($Var0021, 560, 370, 192, 125, -1, BitOR($Var0007, $Var0008))
GUICtrlCreateTab(0, 0, 560, 370)
$Var0022 = GUICtrlCreateTabItem("Compiler")
$Var0023 = GUICtrlCreateGroup("Main", 8, 25, 289, 105)
$Var0024 = GUICtrlCreateLabel("Source:", 21, 52, 41, 17)
$Var0025 = GUICtrlCreateLabel("Dest:", 21, 75, 36, 17)
$Var0026 = GUICtrlCreateLabel("Icon:", 21, 97, 28, 17)
$Var0027 = GUICtrlCreateInput("", 69, 46, 175, 22)
GUICtrlSetState(-1, $Var0003)
$Var0028 = GUICtrlCreateInput("", 69, 70, 175, 22)
$Var0029 = GUICtrlCreateInput("", 69, 94, 175, 22)
GUICtrlSetState(-1, $Var0003)
$Var002A = GUICtrlCreateButton("...", 245, 45, 41, 23)
$Var002B = GUICtrlCreateButton("...", 245, 69, 41, 23)
$Var002C = GUICtrlCreateButton("...", 245, 93, 41, 23)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$Var002D = GUICtrlCreateGroup("Options", 304, 25, 249, 281)
$Var002E = GUICtrlCreateCheckbox("Use BETA include", 434, 148, 105, 17)
GUICtrlSetTip(-1, "Use Beta include file of AutoIT" & $Var0015)
$Var002F = GUICtrlCreateLabel("Execute Level:", 312, 276, 75, 17)
$Var0030 = GUICtrlCreateCheckbox("Use Obfuscate", 312, 41, 105, 25)
GUICtrlSetTip(-1, "This option will Obfuscate your script before , write by LemonXTT.")
$Var0031 = GUICtrlCreateCheckbox("AntiPDebug", 312, 80, 80, 17)
GUICtrlSetTip(-1, "Warning: If your code use parament like /AutoIt3ExecuteLine..., uncheck this option!")
$Var0032 = GUICtrlCreateCheckbox("Add Fake Script", 434, 115, 100, 17)
GUICtrlSetTip(-1, "Add Fake script at Overlay of Compiled file.")
$Var0033 = GUICtrlCreateCheckbox("Allow Decompile", 312, 148, 105, 17)
GUICtrlSetTip(-1, "Allow to decompile application by using Rebuilder tab and set password to rebuild.")
$Var0034 = GUICtrlCreateCheckbox("UPX", 434, 41, 49, 25)
GUICtrlSetTip(-1, "Recommend, Fast and high ratio Compressor.")
$Var0035 = GUICtrlCreateCheckbox("MPRESS", 434, 76, 73, 25)
GUICtrlSetTip(-1, "Not Recommend, Strong ratio Compressor but most antivirus false alarm!")
$Var0036 = GUICtrlCreateCheckbox("Compile for x64", 312, 115, 98, 17)
GUICtrlSetTip(-1, "Compile for x64 OS Architecture.")
$Var0037 = GUICtrlCreateCombo("asInvoker", 392, 273, 153, 25, BitOR($Var000A, $Var0009, 7))
GUICtrlSetData(-1, "requireAdministrator|highestAvailable")
GUICtrlSetTip(-1, "Add manifest to Compiled file.")
GUICtrlCreateGroup("", -99, -99, 1, 1)
$Var0038 = GUICtrlCreateGroup("File Info", 8, 137, 289, 169)
$Var0039 = GUICtrlCreateLabel("CompanyName:", 16, 157, 79, 17)
$Var003A = GUICtrlCreateLabel("Description:", 16, 205, 60, 17)
$Var003B = GUICtrlCreateLabel("Version:", 16, 229, 42, 17)
$Var003C = GUICtrlCreateLabel("ProductName:", 16, 254, 72, 17)
$Var003D = GUICtrlCreateLabel("ProductVersion:", 16, 278, 79, 17)
$Var003E = GUICtrlCreateLabel("LegalCopyright", 16, 181, 73, 17)
$Var003F = GUICtrlCreateInput("", 104, 153, 185, 22)
$Var0040 = GUICtrlCreateInput("� " & @YEAR, 104, 177, 185, 22)
$Var0041 = GUICtrlCreateInput("", 104, 201, 185, 22)
$Var0042 = GUICtrlCreateInput("1.0.0.0", 104, 225, 185, 22)
$Var0043 = GUICtrlCreateInput("", 104, 249, 185, 22)
$Var0044 = GUICtrlCreateInput("1.0.0.0", 104, 273, 185, 22)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$Var0045 = GUICtrlCreateButton("Compile", 6, 309, 473, 41)
$Var0046 = GUICtrlCreateProgress(7, 352, 547, 11)
GUICtrlSetTip(-1, "Are you ready?")
$Var0047 = GUICtrlCreateButton("About", 481, 309, 73, 41)
$Var0048 = GUICtrlCreateTabItem("Rebuilder")
GUICtrlSetState(-1, $Var0004)
$Var0049 = GUICtrlCreateInput("", 185, 137, 209, 22)
GUICtrlSetState(-1, $Var0003)
$Var004A = GUICtrlCreateButton("...", 401, 137, 49, 21)
$Var004B = GUICtrlCreateInput("", 185, 169, 265, 22)
$Var004C = GUICtrlCreateButton("Rebuild", 185, 201, 265, 33)
$Var004D = GUICtrlCreateLabel("File:", 128, 141, 41, 17)
$Var004E = GUICtrlCreateLabel("Password:", 128, 173, 57, 17)
GUISetState(@SW_SHOW)
GUICtrlSetState($Var0022, $Var0004)
WinSetTrans($Var0021, "", 240)
WinSetOnTop($Var0021, "", 1)
If $CMDLINE[0] = 1 Then
	If $CMDLINE[1] = "RANDOMALLCHAR" Then $Var0017 = True
ElseIf Not @Compiled Then
	$Var0017 = True
Else
	$Var0017 = False
EndIf
While 1
	$Var004F = GUIGetMsg()
	Switch $Var004F
		Case $Var0000
			If GUICtrlRead($Var0027) <> "" Then
				If Not StringInStr(FileRead(GUICtrlRead($Var0027)), "[CWAutCompFileInfo]") Then
					If MsgBox(36, "", "Write FileInfo to source?", Default, $Var001E) = 6 Then
						Func0012()
					EndIf
				EndIf
			EndIf
			Func000A(1)
		Case $Var0001
			If @GUI_DropId = $Var0027 Then
				If StringInStr(FileRead(GUICtrlRead($Var0027)), "[CWAutCompFileInfo]") Then
					If StringInStr(FileRead(GUICtrlRead($Var0027)), "#requireadmin") Then GUICtrlSetData($Var0037, "requireAdministrator")
					Func000D()
				EndIf
				GUICtrlSetData($Var0028, "")
			EndIf
		Case $Var0033
			If GUICtrlRead($Var0033) = 1 Then
				$Var001F = InputBox("Password", "Set password to decompile this application.", "", Default, Default, Default, Default, Default, Default, $Var001E)
				If $Var001F = "" Or @error Then GUICtrlSetState($Var0033, $Var0002)
			EndIf
		Case $Var002A
			$Var0050 = FileOpenDialog("Choose Source", @DesktopDir, "AutoIT3(*.au3)", "", "", $Var001E)
			If Not @error Then
				GUICtrlSetData($Var0027, $Var0050)
				If StringInStr(FileRead(GUICtrlRead($Var0027)), "#requireadmin") Then GUICtrlSetData($Var0037, "requireAdministrator")
				Func000D()
			EndIf
		Case $Var002B
			$Var0051 = FileSaveDialog("Save as", @DesktopDir, "Executable(*.exe)", "", "", $Var001E)
			If Not @error Then
				If Not StringInStr(StringRight($Var0051, 4), ".exe") Then $Var0051 = $Var0051 & ".exe"
				GUICtrlSetData($Var0028, $Var0051)
			Else
				GUICtrlSetData($Var0028, "")
			EndIf
		Case $Var002C
			GUICtrlSetData($Var0029, FileOpenDialog("Choose Icon", @DesktopDir, "Icon(*.ico)", "", "", $Var001E))
		Case $Var0034
			GUICtrlSetState($Var0035, $Var0002)
		Case $Var0035
			GUICtrlSetState($Var0034, $Var0002)
		Case $Var0047
			MsgBox(64, "About", "CWAutComp v" & $Var0014 & @CRLF & "Author: code_war_509" & @CRLF & "Release date: " & $Var0016 & @CRLF & "AutoIt Engine: " & $Var0015 & @CRLF & "Thanks to:" & @CRLF & "+ Jonathan Bennett & AutoIT Team for awesome script and converter." & @CRLF & "+ LemonXTT@hocautoit helped me fix some errors, write some useful functions and write Obfuscate." & @CRLF & "+ Angus Johnson for Resource Hacker." & @CRLF & "+ ddbug@codeproject for VerPatch." & @CRLF & "+ vic4key for ChecksumFix, TimeDateStamp." & @CRLF & "+ Igor Pavlov for 7z." & @CRLF & "+ endlesslove_1998@hocautoit." & @CRLF & "+ Ward@autoitscript." & @CRLF & "+ UPX and MPRESS for great exe compressor." & @CRLF & "+ All tester on hocautoit Forum" & @CRLF & @CRLF & "For bugs report or feature request please sent email to: code_war_509@hotmail.com", Default, $Var001E)
		Case $Var0045
			WinSetOnTop($Var0021, "", 0)
			Func000A()
			If Not GUICtrlRead($Var0027) = "" Then
				If GUICtrlRead($Var0042) = "" Then
					MsgBox(16, "Hmmm...", "FileVersion Need!", Default, $Var001E)
				Else
					If Not Func0009(GUICtrlRead($Var0044)) Or Not Func0009(GUICtrlRead($Var0042)) Then
						MsgBox(16, "Hmmm...", "Badly formatted ProductVersion or FileVersion.", Default, $Var001E)
					Else
						If GUICtrlRead($Var0044) = "" Then
							MsgBox(16, "Hmmm...", "ProductVersion Need!", Default, $Var001E)
						Else
							If StringRight(GUICtrlRead($Var0027), 4) <> ".au3" Then
								MsgBox(16, "Hmmm...", "Not .au3 file ext will get error in some case...", Default, $Var001E)
							Else
								GUICtrlSetState($Var0027, $Var0006)
								GUICtrlSetState($Var0045, $Var0006)
								Func0003()
								Func000A()
								GUICtrlSetState($Var0027, $Var0005)
								GUICtrlSetState($Var0045, $Var0005)
							EndIf
						EndIf
					EndIf
				EndIf
			Else
				WinSetOnTop($Var0021, "", 0)
				MsgBox(16, "Hmmm...", "Source is IMPORTANT!", Default, $Var001E)
			EndIf
		Case $Var004A
			$Var0052 = FileOpenDialog("Choose file Compiled with CWAutComp", @DesktopDir, "Executable (*.exe)", Default, "", $Var001E)
			If Not @error Then GUICtrlSetData($Var0049, $Var0052)
		Case $Var004C
			If Not FileExists(GUICtrlRead($Var0049)) Then
				MsgBox(16, "STOP!", "File not exists!", Default, $Var001E)
			Else
				If Not StringInStr(StringToBinary(FileRead(GUICtrlRead($Var0049))), "4300570041005500540043004F004D0050") Then
					MsgBox(16, "STOP!", "This file not compiled by CWAutComp or has been modified resource name.", Default, $Var001E)
				Else
					If GUICtrlRead($Var004B) = "" Then
						MsgBox(16, "STOP!", "Enter Password!", Default, $Var001E)
					Else
						Func0006()
					EndIf
				EndIf
			EndIf
	EndSwitch
WEnd
Func Func0002($Var0053)
	Local $Var0054
	For $Var0055 = 1 To $Var0053
		If Random(1, 2, 1) = 1 Then
			$Var0054 &= Chr(Random(65, 70, 1))
		Else
			$Var0054 &= Chr(Random(48, 57, 1))
		EndIf
		If Random(1, 2, 1) = 1 Then
			$Var0054 &= Chr(Random(65, 70, 1))
		Else
			$Var0054 &= Chr(Random(48, 57, 1))
		EndIf
	Next
	Return $Var0054
EndFunc
Func Func0003()
	Func000E("Initializing...")
	If GUICtrlRead($Var002E) = 1 Then
		FileInstall("7z.exe", @TempDir & "\CWAutComp\7z.exe", 1)
		FileInstall("Include.7z", @TempDir & "\CWAutComp\Include.7z", 1)
		RunWait('"' & @TempDir & '\CWAutComp\7z.exe" x "' & @TempDir & '\CWAutComp\Include.7z"', @TempDir & "\CWAutComp", @SW_HIDE)
		DirCreate(@TempDir & "\CWAutComp\Aut2Exe")
		$Var0019 = @TempDir & "\CWAutComp\Aut2Exe"
	Else
		$Var0019 = $Var0020
	EndIf
	FileInstall("mpress.exe", @TempDir & "\CWAutComp\mpress.exe", 1)
	FileInstall("upx.exe", @TempDir & "\CWAutComp\upx.exe", 1)
	FileInstall("CWAut2Exe.exe", $Var0019 & "\CWAut2Exe.exe", 1)
	FileInstall("fcs.exe", @TempDir & "\CWAutComp\ChecksumFixer.exe", 1)
	FileInstall("ResHacker.exe", @TempDir & "\CWAutComp\ResHacker.exe", 1)
	FileInstall("verpatch.exe", @TempDir & "\CWAutComp\verpatch.exe", 1)
	FileInstall("tds.exe", @TempDir & "\CWAutComp\tds.exe", 1)
	If GUICtrlRead($Var0030) = 1 Then
		Func000E("Obfuscating...")
		If Func0014(GUICtrlRead($Var0027), StringTrimRight(GUICtrlRead($Var0027), 4) & "_OBFS.au3") = 5 Then
			Func000A()
			MsgBox(16, "ERROR", "Obfuscate ERROR", Default, $Var001E)
			Func000E("")
			Return
		Else
			$Var0056 = StringTrimRight(GUICtrlRead($Var0027), 4) & "_OBFS.au3"
		EndIf
	Else
		$Var0056 = GUICtrlRead($Var0027)
	EndIf
	If GUICtrlRead($Var0036) = 1 Then
		FileInstall("CWStub_x64.exe", @TempDir & "\CWAutComp\CWStub.exe", 1)
	Else
		FileInstall("CWStub_x86.exe", @TempDir & "\CWAutComp\CWStub.exe", 1)
	EndIf
	GUICtrlSetData($Var0046, 10)
	Func000E("Editing Stub...")
	Func0005()
	If GUICtrlRead($Var0028) = "" Then GUICtrlSetData($Var0028, StringTrimRight(GUICtrlRead($Var0027), 3) & "exe")
	GUICtrlSetData($Var0046, 20)
	Func000E("Compiling...")
	$Var0057 = '/in "' & ($Var0056) & '" /out "' & @TempDir & '\CWAutComp\aTemp.a3x" /comp 4 /nopack'
	GUICtrlSetData($Var0046, 35)
	$Var0058 = RunWait('"' & $Var0019 & '\CWAut2Exe.exe" ' & $Var0057)
	FileDelete($Var0018)
	If $Var0058 = 0 Then
		If Not GUICtrlRead($Var0029) = "" Then
			Func000E("Patching icon...")
			Func000C()
		EndIf
		If GUICtrlRead($Var0037) <> "asInvoker" Then
			Func000E("Patching Manifest...")
			Func000B()
		EndIf
		If GUICtrlRead($Var0031) = 1 Then
			Func000E("Patching Stub Parament...")
			Func0010()
		EndIf
		Func000E("Patching Version Info...")
		Func000F()
		Func0004()
	Else
		Func000A()
		MsgBox(16, "ERROR", "Aut2Exe Exitted with ERROR", Default, $Var001E)
		GUICtrlSetData($Var0046, 0)
		GUICtrlSetState($Var0027, $Var0005)
		GUICtrlSetState($Var0045, $Var0005)
		Func000E("")
		Return
	EndIf
EndFunc
Func Func0004()
	Func000E("Adding resource...")
	If Not FileExists(@TempDir & "\CWAutComp\aTemp.a3x") Then
		Func000A()
		MsgBox(16, "ERROR", "Cannot find Compiled file, maybe Aut2Exe error or try compile again!", Default, $Var001E)
		Func000E("")
		GUICtrlSetData($Var0046, 0)
		GUICtrlSetState($Var0027, $Var0005)
		GUICtrlSetState($Var0045, $Var0005)
	Else
		If GUICtrlRead($Var0033) = 1 Then
			$Var0059 = Func0001("|") & Hex(Func0007($Var001B, $Var001F))
		Else
			$Var0059 = ""
		EndIf
		If Not $Var0017 Then
			$Var005A = Func0000($Var001B)
		Else
			$Var005A = ""
		EndIf
		$Var005B = "0x" & StringTrimLeft(StringTrimRight(Binary(FileRead(@TempDir & "\CWAutComp\aTemp.a3x")), 14), 4) & Func0001("|") & $Var005A & $Var0059
		FileWrite(@TempDir & "\CWAutComp\TEMP", BinaryToString($Var005B))
		GUICtrlSetData($Var0046, 50)
		ShellExecuteWait(@TempDir & "\CWAutComp\ResHacker.exe", '-add "' & @TempDir & "\CWAutComp\CWStub.exe" & '", "' & @TempDir & "\CWAutComp\release.exe" & '", "' & @TempDir & "\CWAutComp\TEMP" & '", RCDATA, CWAUTCOMP, 0')
		If FileExists(@TempDir & "\CWAutComp\release.exe") Then
			GUICtrlSetData($Var0046, 80)
			If GUICtrlRead($Var0032) = 1 Then
				Func000E("Adding Fake Script...")
				Func0013()
			EndIf
			Func0008()
		Else
			Func000A()
			MsgBox(16, "ERROR", "Something went WRONG!", Default, $Var001E)
			GUICtrlSetData($Var0046, 0)
			GUICtrlSetState($Var0027, $Var0005)
			GUICtrlSetState($Var0045, $Var0005)
		EndIf
	EndIf
EndFunc
Func Func0005()
	Global $Var001B = Func0002(4), $Var001A = Func0002(1), $Var005C = Func0002(7), $Var005D = Func0002(8), $Var005E = Func0002(4), $Var005F = Func0002(4)
	$Var0060 = StringToBinary(FileRead($Var0019 & "\CWAut2Exe.exe"))
	$Var0060 = StringReplace(StringReplace($Var0060, "CB5EF0B4", $Var001B), "35303900", "353039" & $Var001A)
	If $Var0017 Then $Var0060 = StringReplace(StringReplace(StringReplace(StringReplace($Var0060, "4357417574436F", $Var005C), "6D70A9636F64655F", $Var005D), "7761725F", $Var005E), "353039" & $Var001A, $Var005F)
	$Var0061 = FileOpen($Var0019 & "\CWAut2Exe.exe", 2)
	FileWrite($Var0061, BinaryToString($Var0060))
	FileClose($Var0061)
	$Var0062 = StringToBinary(FileRead(@TempDir & "\CWAutComp\CWStub.exe"))
	$Var0062 = StringReplace(StringReplace($Var0062, "CB5EF0B4", $Var001B), "35303900", "353039" & $Var001A)
	If $Var0017 Then
		$Var0062 = StringReplace(StringReplace(StringReplace(StringReplace($Var0062, "4357417574436F", $Var005C), "6D70A9636F64655F", $Var005D), "7761725F", $Var005E), "353039" & $Var001A, $Var005F)
		$Var0062 = StringReplace($Var0062, "54686973206973206120636F6D70696C6564204175746F4974207363726970742E20415620726573656172636865727320706C6561736520656D61696C206176737570706F7274406175746F69747363726970742E636F6D20666F7220737570706F72742E", "52756E74696D65206572726F7221000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")
	Else
		$Var0062 = StringReplace($Var0062, "54686973206973206120636F6D70696C6564204175746F4974207363726970742E20415620726573656172636865727320706C6561736520656D61696C206176737570706F7274406175746F69747363726970742E636F6D20666F7220737570706F72742E", "436F6D70696C6564204175746F495420536372697074206279204357417574436F6D702076" & StringTrimLeft(StringToBinary($Var0014), 2) & "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")
	EndIf
	$Var0063 = FileOpen(@TempDir & "\CWAutComp\CWStub.exe", 2)
	FileWrite($Var0063, BinaryToString($Var0062))
	FileClose($Var0063)
	RunWait('"' & @TempDir & '\CWAutComp\ChecksumFixer.exe" "' & $Var0019 & '\CWAut2Exe.exe"', "", @SW_HIDE)
EndFunc
Func Func0006()
	Local $Var0064
	Func000E("Initializing...")
	DirCreate(@TempDir & "\CWAutRebuild\")
	FileInstall("reshacker.exe", @TempDir & "\CWAutRebuild\reshacker.exe")
	Func000E("Extracting resource...")
	RunWait('"' & @TempDir & "\CWAutRebuild\reshacker.exe" & '" -extract "' & GUICtrlRead($Var0049) & '", "' & @TempDir & '\CWAutRebuild\res.rc", RCDATA,CWAUTCOMP,')
	$Var0064 = FileRead(@TempDir & "\CWAutRebuild\RCData_1.bin")
	Func000E("Checking file...")
	If Not StringInStr(StringToBinary($Var0064), "4357417574436F6D70") Then
		MsgBox(16, "STOP!", "Invaild Header or File not found!", Default, $Var001E)
		FileDelete(@TempDir & "\CWAutRebuild\")
		Func000E("")
		Return
	EndIf
	$Var0065 = Func0000(StringLeft(StringRight(StringToBinary($Var0064), 26), 8))
	Func000E("Checking password...")
	$Var0066 = Func0007($Var0065, GUICtrlRead($Var004B))
	$Var0067 = "0x" & StringRight(StringToBinary($Var0064), 16)
	If $Var0066 <> $Var0067 Then
		MsgBox(16, "STOP!", "This password won't work :P try again :P", Default, $Var001E)
		FileDelete(@TempDir & "\CWAutRebuild\")
		Func000E("")
		Return
	EndIf
	Func000E("Moving...")
	FileWrite(GUICtrlRead($Var0049) & "_Rebuilded", BinaryToString(StringReplace(StringReplace(StringToBinary($Var0064), "4357417574436F6D70A9636F64655F7761725F353039", "A3484BBE986C4AA9994C530A86D6487D4155332145413036"), $Var0065, "BCAD0000")))
	FileDelete(@TempDir & "\CWAutRebuild\")
	Func000E("OK")
	MsgBox(64, "Compelete", "Rebuilded, now put to myaut2exe and find offset ;)", Default, $Var001E)
	Func000E("")
EndFunc
Func Func0007($Var0068, $Var0069)
	Local $Var006A = "0xC81001006A006A005356578B551031C989C84989D7F2AE484829C88945F085C00F84DC000000B90001000088C82C0188840DEFFEFFFFE2F38365F4008365FC00817DFC000100007D478B45FC31D2F775F0920345100FB6008B4DFC0FB68C0DF0FEFFFF01C80345F425FF0000008945F48B75FC8A8435F0FEFFFF8B7DF486843DF0FEFFFF888435F0FEFFFFFF45FCEBB08D9DF0FEFFFF31FF89FA39550C76638B85ECFEFFFF4025FF0000008985ECFEFFFF89D80385ECFEFFFF0FB6000385E8FEFFFF25FF0000008985E8FEFFFF89DE03B5ECFEFFFF8A0689DF03BDE8FEFFFF860788060FB60E0FB60701C181E1FF0000008A840DF0FEFFFF8B750801D6300642EB985F5E5BC9C21000"
	Local $Var006B = DllStructCreate("byte[" & BinaryLen($Var006A) & "]")
	DllStructSetData($Var006B, 1, $Var006A)
	Local $Var006C = DllStructCreate("byte[" & BinaryLen($Var0068) & "]")
	DllStructSetData($Var006C, 1, $Var0068)
	DllCall("user32.dll", "none", "CallWindowProc", "ptr", DllStructGetPtr($Var006B), "ptr", DllStructGetPtr($Var006C), "int", BinaryLen($Var0068), "str", $Var0069, "int", 0)
	Local $Var0058 = DllStructGetData($Var006C, 1)
	$Var006C = 0
	$Var006B = 0
	Return $Var0058
EndFunc
Func Func0008()
	If GUICtrlRead($Var0034) = 1 Then
		Func000E("UPX Packing...")
		RunWait('"' & @TempDir & '\CWAutComp\upx.exe" -9 --keep-resource=10/CWAUTCOMP "release.exe"', @TempDir & "\CWAutComp", @SW_HIDE)
	ElseIf GUICtrlRead($Var0035) = 1 Then
		Func000E("MPRESS Packing...")
		RunWait('"' & @TempDir & '\CWAutComp\mpress.exe" -i -r -s "release.exe"', @TempDir & "\CWAutComp", @SW_HIDE)
	EndIf
	Func000E("Moving to Dest...")
	If FileExists(GUICtrlRead($Var0028)) Then
		If MsgBox(49, "Warning", "Dest file exists, if you continue i will overwrite file!", Default, $Var001E) <> 1 Then
			Func000A()
			GUICtrlSetData($Var0046, 0)
			MsgBox(64, "", "Progress cancelled!", Default, $Var001E)
			Func000E("")
			Return
		EndIf
	EndIf
	If FileMove(@TempDir & "\CWAutComp\release.exe", GUICtrlRead($Var0028), 1) = 0 Then
		Func000A()
		MsgBox(16, "ERROR", "Can't create compiled file at:" & @CRLF & GUICtrlRead($Var0028), Default, $Var001E)
		GUICtrlSetState($Var0027, $Var0005)
		GUICtrlSetState($Var0045, $Var0005)
		GUICtrlSetData($Var0046, 0)
	Else
		Func000E("Fixing TimeDateStamp, Checksum...")
		RunWait('"' & @TempDir & '\CWAutComp\tds.exe" "' & GUICtrlRead($Var0028) & '"', "", @SW_HIDE)
		RunWait('"' & @TempDir & '\CWAutComp\ChecksumFixer.exe" "' & GUICtrlRead($Var0028) & '"', "", @SW_HIDE)
		Func000A()
		GUICtrlSetData($Var0046, 100)
		Func000E("Finished.")
		MsgBox(64, "All Right!", "Completed!", Default, $Var001E)
		Func000E("")
		GUICtrlSetState($Var0027, $Var0005)
		GUICtrlSetState($Var0045, $Var0005)
		GUICtrlSetData($Var0046, 0)
		WinSetOnTop($Var0021, "", 1)
	EndIf
	Return
EndFunc
Func Func0009($Var006D)
	If StringRight($Var006D, 1) = "." Or StringLeft($Var006D, 1) = "." Then Return False
	For $Var006E = 1 To StringLen($Var006D)
		$Var006F = StringMid($Var006D, $Var006E, 1)
		If StringRegExp($Var006F, "\d") = False And StringRegExp($Var006F, "\.") = False Then Return False
	Next
	Return True
EndFunc
Func Func000A($Var0070 = 0)
	FileDelete(StringTrimRight(GUICtrlRead($Var0027), 4) & "_OBFS.au3")
	FileDelete($Var0019 & "\CWAut2Exe.exe")
	FileDelete(@TempDir & "\CWAutComp")
	FileDelete(@TempDir & "\CWAutComp\Aut2Exe")
	FileDelete(@TempDir & "\CWAutComp\Include")
	DirCreate(@TempDir & "\CWAutComp")
	If $Var0070 Then
		DirRemove(@TempDir & "\CWAutComp\Aut2Exe")
		DirRemove(@TempDir & "\CWAutComp\Include")
		FileDelete(@TempDir & "\CWAutComp")
		DirRemove(@TempDir & "\CWAutComp")
		Exit
	EndIf
EndFunc
Func Func000B()
	Local $Var0071
	$Var0071 = '<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">' & @CRLF
	$Var0071 &= " 	<dependency>" & @CRLF
	$Var0071 &= " 		<dependentAssembly>" & @CRLF
	$Var0071 &= ' 			<assemblyIdentity type="win32" name="Microsoft.Windows.Common-Controls" version="6.0.0.0" language="*" processorArchitecture="*" publicKeyToken="6595b64144ccf1df"/>' & @CRLF
	$Var0071 &= " 		</dependentAssembly>" & @CRLF
	$Var0071 &= " 	</dependency>" & @CRLF
	$Var0071 &= ' 	<trustInfo xmlns="urn:schemas-microsoft-com:asm.v3">' & @CRLF
	$Var0071 &= " 		<security>" & @CRLF
	$Var0071 &= " 			<requestedPrivileges>" & @CRLF
	$Var0071 &= ' 				<requestedExecutionLevel level="' & GUICtrlRead($Var0037) & '" uiAccess="false"/>' & @CRLF
	$Var0071 &= " 			</requestedPrivileges>" & @CRLF
	$Var0071 &= " 		</security>" & @CRLF
	$Var0071 &= " 	</trustInfo>" & @CRLF
	$Var0071 &= '	<compatibility xmlns="urn:schemas-microsoft-com:compatibility.v1">' & @CRLF
	$Var0071 &= " 		<application>" & @CRLF
	$Var0071 &= ' 			<supportedOS Id="{e2011457-1546-43c5-a5fe-008deee3d3f0}"/>' & @CRLF
	$Var0071 &= '			<supportedOS Id="{35138b9a-5d96-4fbd-8e2d-a2440225f93a}"/>' & @CRLF
	$Var0071 &= '			<supportedOS Id="{4a2f28e3-53b9-4441-ba9c-d69d4a4a6e38}"/>' & @CRLF
	$Var0071 &= '			<supportedOS Id="{1f676c76-80e1-4239-95bb-83d0f6d0da78}"/>' & @CRLF
	$Var0071 &= "		</application>" & @CRLF
	$Var0071 &= " 	</compatibility>" & @CRLF
	$Var0071 &= "</assembly>"
	FileWrite(@TempDir & "\CWAutComp\Manifest.xml", $Var0071)
	$Var0072 = ' -modify "CWStub.exe", "CWStub.exe", "' & @TempDir & '\CWAutComp\Manifest.xml", 24, 1, 2057'
	RunWait('"' & @TempDir & '\CWAutComp\ResHacker.exe"' & $Var0072, @TempDir & "\CWAutComp", @SW_HIDE)
EndFunc
Func Func000C()
	$Var0073 = ' -modify "CWStub.exe", "CWStub.exe", "' & GUICtrlRead($Var0029) & '", ICONGROUP, 99, 0'
	RunWait('"' & @TempDir & '\CWAutComp\ResHacker.exe"' & $Var0073, @TempDir & "\CWAutComp", @SW_HIDE)
EndFunc
Func Func000D()
	$Var0074 = FileRead(GUICtrlRead($Var0027))
	GUICtrlSetData($Var0041, Func0018($Var0074, "Description=(.*?)\r"))
	GUICtrlSetData($Var0040, Func0018($Var0074, "Copyright=(.*?)\r"))
	GUICtrlSetData($Var003F, Func0018($Var0074, "Company=(.*?)\r"))
	GUICtrlSetData($Var0042, Func0018($Var0074, "Version=(.*?)\r"))
	GUICtrlSetData($Var0043, Func0018($Var0074, "ProductName=(.*?)\r"))
	GUICtrlSetData($Var0044, Func0018($Var0074, "ProductVersion=(.*?)\r"))
EndFunc
Func Func000E($Var0075)
	If $Var0075 <> "" Then
		$Var0076 = $Var0021 & " - " & $Var0075
		WinSetTitle($Var001E, "", $Var0076)
	Else
		$Var0021 = "CWAutComp v" & $Var0014
		WinSetTitle($Var001E, "", $Var0021)
	EndIf
EndFunc
Func Func000F()
	$Var0077 = " /va /langid 2057 CWStub.exe " & GUICtrlRead($Var0042) & ' /s desc "' & GUICtrlRead($Var0041) & '" /s company "' & GUICtrlRead($Var003F) & '" /s (c) "' & GUICtrlRead($Var0040) & '" /s product "' & GUICtrlRead($Var0043) & '" /pv "' & GUICtrlRead($Var0044) & '" /s OriginalFilename "' & Func0016(GUICtrlRead($Var0028)) & '" /s InternalName "' & Func0016(GUICtrlRead($Var0028))
	RunWait('"' & @TempDir & '\CWAutComp\verpatch.exe"' & $Var0077, @TempDir & "\CWAutComp", @SW_HIDE)
EndFunc
Func Func0010()
	$Var0078 = StringToBinary(FileRead(@TempDir & "\CWAutComp\CWStub.exe"))
	$Var0079 = StringReplace($Var0078, "2F004100750074006F0049007400330045007800650063007500740065004C0069006E0065", "2F004100110074006F0049001700330045007800650063007500740065004C0069006E0011")
	$Var007A = StringReplace($Var0079, "2F004100750074006F004900740033004F0075007400700075007400440065006200750067", "2F007700110074006F0049001100330065007800650063007500740065004C0011006E0046")
	FileDelete(@TempDir & "\CWAutComp\CWStub.exe")
	Sleep(300)
	FileWrite(@TempDir & "\CWAutComp\CWStub.exe", BinaryToString($Var007A))
EndFunc
Func Func0011($Var007B)
	DllCall("kernel32.dll", "int", "CreateSemaphore", "int", 0, "long", 1, "long", 1, "str", $Var007B)
	Local $Var007C = DllCall("kernel32.dll", "int", "GetLastError")
	If $Var007C[0] = 183 Then
		MsgBox(16, "ERROR", "Already running!")
		Exit -1
	EndIf
EndFunc
Func Func0012()
	Local $Var0074 = FileRead(GUICtrlRead($Var0027))
	$Var007D = "#cs" & @CRLF & "	[CWAutCompFileInfo]" & @CRLF
	$Var007D &= "	Company=" & GUICtrlRead($Var003F) & @CRLF
	$Var007D &= "	Copyright=" & GUICtrlRead($Var0040) & @CRLF
	$Var007D &= "	Description=" & GUICtrlRead($Var0041) & @CRLF
	$Var007D &= "	Version=" & GUICtrlRead($Var0042) & @CRLF
	$Var007D &= "	ProductName=" & GUICtrlRead($Var0043) & @CRLF
	$Var007D &= "	ProductVersion=" & GUICtrlRead($Var0044) & @CRLF
	$Var007D &= "#ce" & @CRLF
	If Not StringInStr($Var0074, "[CWAutCompFileInfo]") Then
		$Var0074 = $Var007D & $Var0074
	Else
		$Var0074 = StringRegExpReplace($Var0074, "(?si)#cs \[CWAutCompFileInfo\].*?#ce", $Var007D)
	EndIf
	$Var007E = FileOpen(GUICtrlRead($Var0027), 2 + FileGetEncoding(GUICtrlRead($Var0027)))
	FileWrite($Var007E, $Var0074)
	FileClose($Var007E)
EndFunc
Func Func0013()
	Local $Var007F
	$Var007F &= "0x00000000000000000000000000000000000000000000000051328328DAA562562E8705AFB40DC5AC6B43CA52AFAD0000E6FB2578C8E213F97D1DEDDD7100B0552DAC9AD52815D4F0CF25E4CF118E56C2CE3F70EFB96810F80000F0E304D3D0B51941F479192034B8D9BE4AA407B00152FF885892FA84B91ACF6D39F28D9D1E323CB42E26FA734DE47450CDF0E49196EF0D5EC490C1148E398F59405F7137BF8CBDE8DC4DB22733AABCB78AAE77967CA57BEEEB7378A2367C6DD7008387000083870000A9AE6FC9E282CE0196080990E282CE01B756099029BACA35828D3EB9517C58CB3C23C449781BB3CC3C8155AD6BC075B5F6323F347262"
	$Var007F &= "C5F5C25C0BC1A99428E607DB20199F869A20E58FFDEC38A300DAA76427"
	Local $Var0080 = FileOpen(@TempDir & "\CWAutComp\release.exe", 1)
	FileWriteLine($Var0080, BinaryToString($Var007F))
	FileClose($Var0080)
EndFunc
Func Func0014($Var0081, $Var0082, $Var0083 = False)
	If GUICtrlRead($Var002E) <> 1 Then
		FileInstall("Obfuscator.exe", @TempDir & "\CWAutComp\Obfuscator.exe", 1)
		FileInstall("Obfuscator.dat", @TempDir & "\CWAutComp\Obfuscator.dat", 1)
		$Var0084 = @TempDir & "\CWAutComp\Obfuscator.exe"
	Else
		$Var0084 = $Var0020 & "\SciTE\Obfuscator\Obfuscator.exe"
		If Not FileExists($Var0084) Then
			MsgBox(16, "Obfuscator Faill", "Obfuscator.exe not found, maybe SCITE not installed" & @CRLF & "You may try to check Use BETA Include box and try again!", Default, $Var001E)
			Return 5
		EndIf
	EndIf
	$Var0085 = $Var0081
	$Var0068 = FileRead($Var0085)
	$Var0086 = StringReplace($Var0085, ".au3", "__.au3")
	$Var0087 = $Var0086 & ".tbl"
	$Var0088 = StringReplace($Var0086, ".au3", "_Obfuscated.au3")
	$Var001D = Func0016($Var0085)
	$Var001C = Func0015($Var0085)
	$Var0089 = "(\.|\||\*|\?|\+|\(|\)|\{|\}|\[|\]|\^|\$|\\)"
	$Var008A = StringSplit($Var0068, @CRLF, 1)
	Local $Var008B, $Var008C
	For $Var008D = 1 To UBound($Var008A) - 1
		$Var008C = $Var008A[$Var008D]
		If StringLen($Var008C) > 2047 Then
			MsgBox(16, "Obfuscate ERROR", "Max line length is 2047" & @CRLF & "This line is too long: " & $Var008D, "0", $Var001E)
			Return 5
		Else
			$Var008E = False
			If StringRegExp($Var008C, "(?i)\A *Func *\w*\(") And StringIsASCII($Var008C) Then $Var008E = True
			$Var008F = StringSplit("GUICtrlSetOnEvent|GUISetOnEvent|TraySetOnEvent|FileInstall|#include|#OnAutoItStartRegister|Opt|DllOpen|ObjEvent|DllCallbackRegister|AdlibRegister|AdlibUnRegister|GUIRegisterMsg|eval|guictrlregisterlistviewsort|hotkeyset|isdeclared|onautoitexitunregister|onautoitexitregister|trayitemsetonevent", "|", 1)
			For $Var006E = 1 To UBound($Var008F) - 1
				If StringInStr($Var008C, $Var008F[$Var006E]) Then
					$Var008E = True
					ExitLoop
				EndIf
			Next
			If $Var008E = False Then
				If StringIsASCII($Var008C) And StringLen($Var008C) > 500 Then
				Else
					$Var0090 = StringRegExp($Var008A[$Var008D], "('.*?'|" & '".*?")', 3)
					For $Var006E = 0 To UBound($Var0090) - 1
						If StringRegExp($Var008C, "(?i)\A *Func *\w*\(") And (Not StringIsASCII($Var0090[$Var006E])) Then
							$Var0091 = Func0018($Var008C, "(\$\w*) *= *" & StringRegExpReplace($Var0090[$Var006E], "(\.|\||\*|\?|\+|\(|\)|\{|\}|\[|\]|\^|\$|\\)", "\\1"))
							$Var008C = StringReplace($Var008C, Func0018($Var008C, "\$\w* *= *" & StringRegExpReplace($Var0090[$Var006E], "(\.|\||\*|\?|\+|\(|\)|\{|\}|\[|\]|\^|\$|\\)", "\\1")), $Var0091 & ' = "Default"')
							$Var008C &= @CRLF & "If " & $Var0091 & ' = "Default" then ' & $Var0091 & " = " & "BinaryToString('" & StringToBinary(StringTrimLeft(StringTrimRight($Var0090[$Var006E], 1), 1), 4) & "',4)"
						Else
							If StringRegExp($Var008C, "(?i)\A *Func *\w*\(") And StringIsASCII($Var0090[$Var006E]) Then
							Else
								If $Var0090[$Var006E] <> "''" And $Var0090[$Var006E] <> '""' Then
									$Var008C = StringReplace($Var008C, $Var0090[$Var006E], "BinaryToString('" & StringToBinary(StringTrimLeft(StringTrimRight($Var0090[$Var006E], 1), 1), 4) & "',4)")
									If StringRegExp($Var008C, "BinaryToString\(.*?\)BinaryToString\(.*?\)") Then $Var008C = StringRegExpReplace($Var008C, "(BinaryToString\(.*?\))(BinaryToString\(.*?\))", "\1&\2")
								EndIf
							EndIf
						EndIf
					Next
				EndIf
			EndIf
		EndIf
		$Var008B &= $Var008C & @CRLF
	Next
	$Var0092 = $Var008B
	$Var0061 = FileOpen($Var0086, 2 + 8)
	FileWrite($Var0061, $Var0092)
	FileClose($Var0061)
	$Var0093 = Run('"' & $Var0084 & '" "' & $Var0086 & '" ', "", Default, $Var000C + $Var000B)
	Local $Var0094
	Do
		Sleep(10)
		$Var0095 = StdoutRead($Var0093)
		If $Var0095 <> "" Then $Var0094 &= $Var0095
		$Var0096 = @error
		$Var0097 = StderrRead($Var0093)
		If $Var0097 <> "" Then $Var0094 &= "+> " & $Var0097
		$Var0098 = @error
	Until $Var0096 And $Var0098
	If $Var0083 = False Then FileDelete($Var0086)
	If StringInStr($Var0094, "File contains records longer than") Then
		MsgBox(16, "Obfuscate ERROR", $Var0094, Default, $Var001E)
		Return 5
	EndIf
	If Not FileExists($Var0088) Then
		FileMove(@TempDir & "\CWAutComp\Obfuscator.Log", $Var0085 & "_ObfuscationLogs.txt")
		MsgBox(16, "Obfuscate ERROR", "Obfucated file not found, more infomation at " & $Var001D & "_ObfuscationLogs.txt", Default, $Var001E)
		Return 5
	EndIf
	$Var0068 = FileRead($Var0088)
	$Var0099 = Func0018($Var0068, "(?i)(FileInstall\(.*_.*.tbl',.*\))")
	$Var009A = Func0018($Var0068, "(?i)FileInstall\(['|""""](.*\.tbl)")
	$Var009B = False
	If Not FileExists($Var0087) Then
		$Var009B = True
	Else
		$Var009C = FileRead($Var0087)
		$Var009D = "$" & Func0017()
		$Var009E = "Global " & $Var009D & '= "' & StringLeft($Var009C, 2000) & '"'
		If StringLen($Var009C) > 2000 Then
			For $Var006E = 1 To Int(StringLen($Var009C) / 2000)
				$Var009E &= @CRLF & $Var009D & ' &= "' & StringMid($Var009C, 2000 * $Var006E + 1, 2000) & '"'
			Next
		EndIf
		FileDelete($Var001D & ".tbl")
		$Var009F = Func0017()
		$Var00A0 = Func0018($Var0099, "(\s*)F") & "FileInstall('" & $Var001D & ".tbl' ,@TempDir & '\ " & $Var001D & ".tbl', 1)"
		$Var0068 = StringReplace($Var0068, $Var0099, $Var009E)
		If StringRegExp($Var0068, "(?i)\$Os.*=.*Execute.+", 0) Then
			Local $Var00A1 = Func0018($Var0068, "(?i)\$Os.*=.*Execute.*(BinaryToString\('.*?'\))"), $Var00A2 = 0
			Do
				$Var00A2 += 1
				$Var00A1 = StringReplace(StringReplace(BinaryToString(Func0018($Var00A1, "'(.*?)'")), "Execute(", ""), "))", ")")
				If $Var00A2 > 1000 Then
					MsgBox(16, "Obfuscate ERROR", "ERROR", Default, $Var001E)
					Return 5
				EndIf
			Until StringInStr($Var00A1, "StringSplit")
			$Var00A3 = Func0018($Var00A1, "['|" & '"](.+)["|' & "']")
			$Var0068 = StringReplace($Var0068, Func0018($Var0068, "(?i)\$Os.*=.*Execute.+"), "$Os = StringSplit(" & $Var009D & ',"' & $Var00A3 & '",1)')
		EndIf
		$Var00A4 = Func0018($Var0068, "(?i)global *\$\w.+ *=.*?(\w.+?)\(")
		$Var00A5 = Func0017()
		$Var008A = StringSplit($Var0068, @CR)
		$Var00A6 = Func0017()
		$Var0068 = ""
		For $Var006E = 1 To UBound($Var008A) - 1
			$Var008A[$Var006E] = StringRegExpReplace($Var008A[$Var006E], "(?i)(?<=\W)(Number\()(\$\w*)\)", $Var00A6 & "(\2 , " & Random(99, 999, 1) & ")")
			$Var0068 &= StringRegExpReplace($Var008A[$Var006E], "(?i)(" & $Var00A4 & "\(.*?)\)", "\1, " & Random(99, 999, 1) & ")")
		Next
		$Var0068 = StringRegExpReplace($Var0068, "(?i)(func *" & $Var00A4 & "\W*\()(.*), (\d*)\)", "\1\2, \$" & $Var00A5 & ")")
		$Var00A7 = StringRegExp($Var0068, "(?si)Func \w*\(.*?\)", 3)
		$Var00A5 = Func0017()
		$Var00A8 = "Func " & $Var00A6 & "($" & $Var00A5 & ", $" & Func0017() & ")" & @CRLF
		$Var00A8 &= "Return Number( $" & $Var00A5 & ")" & @CRLF
		$Var00A8 &= "EndFunc"
		$Var0068 = StringReplace($Var0068, $Var00A7[UBound($Var00A7) - 1], $Var00A8 & @CRLF & $Var00A7[UBound($Var00A7) - 1])
		$Var0068 = StringRegExpReplace($Var0068, $Var00A4, Func0017())
		$Var00A5 = Func0017()
		$Var0068 = StringRegExpReplace($Var0068, "(Isdeclared\(')(\w*)", "\1" & $Var00A5 & "", 1)
		$Var0068 = StringRegExpReplace($Var0068, "\$Os", "$" & Func0017())
	EndIf
	$Var00A9 = ""
	$Var00AA = StringRegExp($Var0092, "(#AutoIt3Wrapper.*)\r", 3)
	If Not @error Then $Var00A9 = "#region" & @CRLF
	For $Var006E = 0 To UBound($Var00AA) - 1
		If (Not StringInStr($Var00AA[$Var006E], "Obfuscator")) And (Not StringInStr($Var00AA[$Var006E], "#AutoIt3Wrapper_Version=Beta")) Then
			$Var00A9 &= $Var00AA[$Var006E] & @CRLF
		EndIf
	Next
	$Var00A9 &= "#AutoIt3Wrapper_Version=Beta" & @CRLF
	If UBound($Var00AA) > 0 Then
		$Var00A9 &= "#endregion" & @CRLF & @CRLF
		$Var0068 = $Var00A9 & $Var0068
	EndIf
	$Var0061 = FileOpen($Var0082, 2 + 8)
	FileWrite($Var0061, $Var0068)
	FileClose($Var0061)
	If $Var0083 = False Then FileDelete($Var0088)
	If $Var0083 = False Then FileDelete($Var0087)
EndFunc
Func Func0015($Var00AB)
	$Var0090 = StringRegExp($Var00AB, "(.*)\\", 1)
	If UBound($Var0090) > 0 Then Return $Var0090[0]
EndFunc
Func Func0016($Var00AB)
	$Var0090 = StringRegExp($Var00AB, ".*\\(.*)", 1)
	If UBound($Var0090) > 0 Then Return $Var0090[0]
EndFunc
Func Func0017()
	Local $Var00AC = ""
	For $Var006E = 1 To Random(5, 9, 1)
		$Var00AC &= Chr(Random(97, 122, 1))
	Next
	Return $Var00AC
EndFunc
Func Func0018($Var00AD, $Var0090, $Var00AE = 1)
	Local $Var00AF = StringRegExp($Var00AD, $Var0090, $Var00AE)
	If UBound($Var00AF) > 0 Then Return $Var00AF[0]
	Return ""
EndFunc
