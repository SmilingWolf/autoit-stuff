import re

ScriptFile = open('CWAutComp_v2.16.au3', 'rb')
ScriptData = ScriptFile.read()
ScriptFile.close()

FuncsList = re.findall('Func\ [0-9A-F]{11}\(', ScriptData)
Counter = 0
for Elem in FuncsList:
    Expr = Elem[5:-1]
    ScriptData = ScriptData.replace('%s(' % Expr, 'Func%04X(' % Counter)
    Counter += 1

open('CWAutComp_v2.16.renamed.au3', 'wb').write(ScriptData)
