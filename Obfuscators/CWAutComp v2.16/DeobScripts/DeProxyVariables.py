import re, binascii

ScriptFile = open('CWAutComp_v2.16.deobf1.au3', 'r')
ScriptData = ScriptFile.readlines()
ScriptFile.close()

Line = 0
GlobLines = []
ScriptLines = []
while Line < len(ScriptData):
    if ScriptData[Line].startswith('\tIf Not IsDeclared'):
        Line += 1
        while not ScriptData[Line].startswith('\tEndIf'):
            VarsList = ScriptData[Line][9:].rstrip('\r\n').split(', $')
            for Variable in VarsList:
                if not Variable.startswith('$'):
                    Variable = '$%s' % Variable
                GlobLines.append(r'%s' % Variable)
            Line += 1
        GlobLines.pop()
        Line += 1
    else:
        ScriptLines.append(ScriptData[Line])
        Line += 1

ScriptLines = ''.join(ScriptLines)
for Elem in GlobLines:
    Variable = Elem.split(' = ')[0]
    Value = Elem.split(' = ')[1]
    ScriptLines = ScriptLines.replace(Variable, Value)

NumbersList = re.findall('ZACFHLH\(" .*? ", \d{3}\)', ScriptLines)
for Elem in NumbersList:
    Number = Elem[10:-8]
    ScriptLines = ScriptLines.replace(Elem, Number)
    
BinToStrList = re.findall('BinaryToString\(".*?", 4\)', ScriptLines)
for Elem in BinToStrList:
    NewStr = '%s' % binascii.unhexlify(Elem[18:-5])
    if '"' in NewStr:
        NewStr = '\'%s\'' % NewStr
    else:
        NewStr = '"%s"' % NewStr
    ScriptLines = ScriptLines.replace(Elem, NewStr)
    
MacrosList = re.findall('Execute\(" @.*?\)', ScriptLines)
for Elem in MacrosList:
    Number = Elem[10:-3]
    ScriptLines = ScriptLines.replace(Elem, Number)

open('CWAutComp_v2.16.deobf2.au3', 'wb').write(ScriptLines)