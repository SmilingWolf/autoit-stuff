#!/usr/bin/var python2

def EncDecString(Size, RawString):
    XorKey_l = (Size & 0xFF)
    XorKey_h = ((Size >> 8) & 0xFF)
    tmpBuff = ''

    for pos in xrange(0, len(RawString), 2):
        tmpBuff += chr(ord(RawString[pos]) ^ XorKey_l)
        tmpBuff += chr(ord(RawString[pos + 1]) ^ XorKey_h)

    return tmpBuff

def MakeAu3String(String):
    NewString = ''
    if '"' in String and '\'' in String:
        NewString = ' "%s" ' % String.replace('"', '""')
    elif '"' in String:
        NewString = " '%s' " % String
    else:
        NewString = ' "%s" ' % String
    return NewString

def CountLines(TokensList):
    Lines = 0
    for Token in TokensList:
        if Token[0] == 0x7F:
            Lines += 1
    return Lines
