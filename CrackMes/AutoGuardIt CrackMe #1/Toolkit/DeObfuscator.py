#!/usr/bin/env python2

# Suboptimal, but it works(TM)
# Magic with RegExes, string substitution and stuff
# In particular, take the encrypted Assign variable name,
# create a script with it, use AutoIt to decrypt it and
# substitute it across the script, rinse and repeat until
# there are no more Assign(ements) left to decrypt

import sys, re, subprocess

Script = open(sys.argv[1], 'r').read()

# Take care of Assign(ements)
FullLines = re.findall('Assign\(Chr.*', Script)
while len(FullLines) != 0:
    ObfAssign = FullLines[0]
    AssLine = ObfAssign.rsplit(',', 1)[1]
    VarLine = ObfAssign.rsplit(',', 1)[0][7:]   #.replace('Assign(', '')

    ScriptFile = open('NameDecrypter.au3', 'wb')
    ScriptFile.write('$Assignement = \'%s\'\r\n' % AssLine)
    ScriptFile.write('$test0 = %s\r\n' % VarLine)
    ScriptFile.write('$hFile = FileOpen(\'FuncNames.txt\', 2)\r\n')
    ScriptFile.write('FileWriteLine($hFile, \'Assign("\' & $test0 & \'",\' & $Assignement)\r\n')
    ScriptFile.write('FileClose($hFile)\r\n')
    ScriptFile.close()
    subprocess.call('"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" NameDecrypter.au3', shell=True)

    DeobfAssign = open('FuncNames.txt', 'r').readline().rstrip('\r\n')
    Script = Script.replace(ObfAssign, DeobfAssign)

    Variable = DeobfAssign.split(',')[0][8:-1]  #.lstrip('Assign("').rstrip('"')
    Function = DeobfAssign.split(',')[1][10:-3] #.lstrip(' Execute("').rstrip('"))')
    InsVar = re.compile(re.escape('$' + Variable), re.IGNORECASE)
    Script = InsVar.sub(Function, Script)
    FullLines = re.findall('Assign\(Chr.*', Script)

FullLines = re.findall('^\t{0,}\$.*\ =\ Chr\(.*Random\(.*SRandom\(.*\)', Script, re.MULTILINE)
ScriptFile = open('NameDecrypter.au3', 'wb')
ScriptFile.write('$hFile = FileOpen(\'FuncNames.txt\', 2)\r\n')
for Elem in FullLines:
    ObfAssign = Elem
    VarLine = ObfAssign.split('=', 1)[0].lstrip(' ').rstrip(' ')
    ScriptFile.write(ObfAssign)
    ScriptFile.write('\r\n$Assignement = \'%s\'\r\n' % VarLine)
    ScriptFile.write('FileWriteLine($hFile, $Assignement & \' = "\' & %s & \'"\')\r\n' % VarLine)
ScriptFile.write('FileClose($hFile)\r\n')
ScriptFile.close()
subprocess.call('"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" NameDecrypter.au3', shell=True)
DeobfAssign = open('FuncNames.txt', 'r').readlines()
for Elem in xrange(len(DeobfAssign)):
    InsVar = re.compile(re.escape(FullLines[Elem]))
    Script = InsVar.sub(DeobfAssign[Elem].rstrip('\r\n'), Script)

FullLines = re.findall('^\t{0,}\$.*\ =\ StringLen\(".*"\)', Script, re.MULTILINE)
for Elem in FullLines:
    ValLine = Elem.split('=', 1)[1].lstrip(' ').rstrip(' ')
    DeobfVal = '%d' % (len(ValLine) - len('StringLen("")'))
    InsVar = re.compile(re.escape(ValLine))
    Script = InsVar.sub(DeobfVal, Script)

print Script
    