#!/usr/bin/var python2

import sys, struct, binascii
import Tokeniser, DeTokeniser, AU3Utils

def DeBinaryToString(TokensList):
    Index = 0
    NewTokensList = []
    while Index < len(TokensList):
        Token = TokensList[Index]
        # BINARYTOSTRING("Userstring")
        if Token[0] == 0x31 and Token[2] == 'BINARYTOSTRING' and TokensList[Index + 1][0] == 0x47 and TokensList[Index + 2][0] == 0x36 and TokensList[Index + 3][0] == 0x48:
            String = TokensList[Index + 2][2]
            if String.startswith('0x') == True:
                String = binascii.unhexlify(String[2:])
            String = AU3Utils.MakeAu3String('%s' % String)
            NewToken = Tokeniser.TextToTokensList('%s\r\n' % String, len(String))[0]
            NewTokensList.append(NewToken)
            Index += 3
        else:
            NewTokensList.append(Token)
        Index += 1
    return NewTokensList

if __name__ == '__main__':
    ScriptText = open(sys.argv[1], 'rb').read() + '\r\n'
    TokensList = Tokeniser.TextToTokensList(ScriptText, len(ScriptText))

    # for Token in TokensList:
    #     print Token

    while 1:
        NumTokens = len(TokensList)
        TokensList = DeBinaryToString(TokensList)
        if NumTokens == len(TokensList):
            break

    TokenData = Tokeniser.TokensListToData(TokensList)
    ScriptText = DeTokeniser.DataToText(TokenData)

    ScriptFile = open(sys.argv[2], 'wb')
    ScriptFile.write(ScriptText)
    ScriptFile.close()
