#NoTrayIcon
#RequireAdmin
#Region
	#AutoIt3Wrapper_Res_File_Add=Lic.txt, RT_RCDATA, LIC
	#AutoIt3Wrapper_Res_File_Add=Msg.txt, RT_RCDATA, MSG
#EndRegion
Global Const $RT_ANICURSOR = 21
Global Const $RT_BITMAP = 2
Global Const $RT_CURSOR = 1
Global Const $RT_FONT = 8
Global Const $RT_ICON = 3
Global Const $RT_MENU = 4
Global Const $RT_RCDATA = 10
Global Const $RT_STRING = 6
Global Const $tagRECT = "struct;long Left;long Top;long Right;long Bottom;endstruct"
Global Const $tagGDIPSTARTUPINPUT = "uint Version;ptr Callback;bool NoThread;bool NoCodecs"
Global Const $tagREBARBANDINFO = "uint cbSize;uint fMask;uint fStyle;dword clrFore;dword clrBack;ptr lpText;uint cch;" & "int iImage;hwnd hwndChild;uint cxMinChild;uint cyMinChild;uint cx;handle hbmBack;uint wID;uint cyChild;uint cyMaxChild;" & "uint cyIntegral;uint cxIdeal;lparam lParam;uint cxHeader" & ((@OSVersion = "WIN_XP") ? "" : ";" & $tagRECT & ";uint uChevronState")
Global Const $STR_STRIPALL = 8
Global Const $HGDI_ERROR = Ptr(-1)
Global Const $INVALID_HANDLE_VALUE = Ptr(-1)
Global Const $KF_EXTENDED = 0x0100
Global Const $KF_ALTDOWN = 0x2000
Global Const $KF_UP = 0x8000
Global Const $LLKHF_EXTENDED = BitShift($KF_EXTENDED, 8)
Global Const $LLKHF_ALTDOWN = BitShift($KF_ALTDOWN, 8)
Global Const $LLKHF_UP = BitShift($KF_UP, 8)
Global Const $IMAGE_BITMAP = 0
Global Const $IMAGE_ICON = 1
Global Const $IMAGE_CURSOR = 2
Global Const $LOAD_LIBRARY_AS_DATAFILE = 0x02
Global Const $LR_DEFAULTCOLOR = 0x0000
Func _WinAPI_DeleteObject($hObject)
	Local $aResult = DllCall("gdi32.dll", "bool", "DeleteObject", "handle", $hObject)
	If @error Then Return SetError(@error, @extended, False)
	Return $aResult[0]
EndFunc
Func _WinAPI_DestroyIcon($hIcon)
	Local $aResult = DllCall("user32.dll", "bool", "DestroyIcon", "handle", $hIcon)
	If @error Then Return SetError(@error, @extended, False)
	Return $aResult[0]
EndFunc
Func _WinAPI_FreeLibrary($hModule)
	Local $aResult = DllCall("kernel32.dll", "bool", "FreeLibrary", "handle", $hModule)
	If @error Then Return SetError(@error, @extended, False)
	Return $aResult[0]
EndFunc
Func _WinAPI_GetModuleHandle($sModuleName)
	Local $sModuleNameType = "wstr"
	If $sModuleName = "" Then
		$sModuleName = 0
		$sModuleNameType = "ptr"
	EndIf
	Local $aResult = DllCall("kernel32.dll", "handle", "GetModuleHandleW", $sModuleNameType, $sModuleName)
	If @error Then Return SetError(@error, @extended, 0)
	Return $aResult[0]
EndFunc
Func _WinAPI_LoadImage($hInstance, $sImage, $iType, $iXDesired, $iYDesired, $iLoad)
	Local $aResult, $sImageType = "int"
	If IsString($sImage) Then $sImageType = "wstr"
	$aResult = DllCall("user32.dll", "handle", "LoadImageW", "handle", $hInstance, $sImageType, $sImage, "uint", $iType, "int", $iXDesired, "int", $iYDesired, "uint", $iLoad)
	If @error Then Return SetError(@error, @extended, 0)
	Return $aResult[0]
EndFunc
Func _WinAPI_LoadLibraryEx($sFileName, $iFlags = 0)
	Local $aResult = DllCall("kernel32.dll", "handle", "LoadLibraryExW", "wstr", $sFileName, "ptr", 0, "dword", $iFlags)
	If @error Then Return SetError(@error, @extended, 0)
	Return $aResult[0]
EndFunc
Func _WinAPI_LoadString($hInstance, $iStringID)
	Local $aResult = DllCall("user32.dll", "int", "LoadStringW", "handle", $hInstance, "uint", $iStringID, "wstr", "", "int", 4096)
	If @error Or Not $aResult[0] Then Return SetError(@error + 10, @extended, "")
	Return SetExtended($aResult[0], $aResult[3])
EndFunc
Global Const $tagOSVERSIONINFO = 'struct;dword OSVersionInfoSize;dword MajorVersion;dword MinorVersion;dword BuildNumber;dword PlatformId;wchar CSDVersion[128];endstruct'
Global Const $__WINVER = __WINVER()
Func __WINVER()
	Local $tOSVI = DllStructCreate($tagOSVERSIONINFO)
	DllStructSetData($tOSVI, 1, DllStructGetSize($tOSVI))
	Local $aRet = DllCall('kernel32.dll', 'bool', 'GetVersionExW', 'struct*', $tOSVI)
	If @error Or Not $aRet[0] Then Return SetError(@error, @extended, 0)
	Return BitOR(BitShift(DllStructGetData($tOSVI, 2), -8), DllStructGetData($tOSVI, 3))
EndFunc
Func _WinAPI_DeleteEnhMetaFile($hEmf)
	Local $aRet = DllCall('gdi32.dll', 'bool', 'DeleteEnhMetaFile', 'handle', $hEmf)
	If @error Then Return SetError(@error, @extended, False)
	Return $aRet[0]
EndFunc
Func _WinAPI_RemoveFontMemResourceEx($hFont)
	Local $aRet = DllCall('gdi32.dll', 'bool', 'RemoveFontMemResourceEx', 'handle', $hFont)
	If @error Then Return SetError(@error, @extended, False)
	Return $aRet[0]
EndFunc
Global $__g_hGDIPDll = 0
Global $__g_iGDIPRef = 0
Global $__g_iGDIPToken = 0
Global $__g_bGDIP_V1_0 = True
Func _GDIPlus_BitmapDispose($hBitmap)
	Local $aResult = DllCall($__g_hGDIPDll, "int", "GdipDisposeImage", "handle", $hBitmap)
	If @error Then Return SetError(@error, @extended, False)
	If $aResult[0] Then Return SetError(10, $aResult[0], False)
	Return True
EndFunc
Func _GDIPlus_Shutdown()
	If $__g_hGDIPDll = 0 Then Return SetError(-1, -1, False)
	$__g_iGDIPRef -= 1
	If $__g_iGDIPRef = 0 Then
		DllCall($__g_hGDIPDll, "none", "GdiplusShutdown", "ulong_ptr", $__g_iGDIPToken)
		DllClose($__g_hGDIPDll)
		$__g_hGDIPDll = 0
	EndIf
	Return True
EndFunc
Func _GDIPlus_Startup($sGDIPDLL = Default, $bRetDllHandle = False)
	$__g_iGDIPRef += 1
	If $__g_iGDIPRef > 1 Then Return True
	If $sGDIPDLL = Default Then $sGDIPDLL = "gdiplus.dll"
	$__g_hGDIPDll = DllOpen($sGDIPDLL)
	If $__g_hGDIPDll = -1 Then
		$__g_iGDIPRef = 0
		Return SetError(1, 2, False)
	EndIf
	Local $sVer = FileGetVersion($sGDIPDLL)
	$sVer = StringSplit($sVer, ".")
	If $sVer[1] > 5 Then $__g_bGDIP_V1_0 = False
	Local $tInput = DllStructCreate($tagGDIPSTARTUPINPUT)
	Local $tToken = DllStructCreate("ulong_ptr Data")
	DllStructSetData($tInput, "Version", 1)
	Local $aResult = DllCall($__g_hGDIPDll, "int", "GdiplusStartup", "struct*", $tToken, "struct*", $tInput, "ptr", 0)
	If @error Then Return SetError(@error, @extended, False)
	If $aResult[0] Then Return SetError(10, $aResult[0], False)
	$__g_iGDIPToken = DllStructGetData($tToken, "Data")
	If $bRetDllHandle Then Return $__g_hGDIPDll
	Return SetExtended($sVer[1], True)
EndFunc
Func _GUICtrlMenu_DestroyMenu($hMenu)
	Local $aResult = DllCall("user32.dll", "bool", "DestroyMenu", "handle", $hMenu)
	If @error Then Return SetError(@error, @extended, False)
	Return $aResult[0]
EndFunc
Func _WinAPI_DestroyCursor($hCursor)
	Local $aRet = DllCall('user32.dll', 'bool', 'DestroyCursor', 'handle', $hCursor)
	If @error Then Return SetError(@error, @extended, 0)
	Return $aRet[0]
EndFunc
Func _WinAPI_FindResource($hInstance, $sType, $sName)
	Local $sTypeOfType = 'int', $sTypeOfName = 'int'
	If IsString($sType) Then
		$sTypeOfType = 'wstr'
	EndIf
	If IsString($sName) Then
		$sTypeOfName = 'wstr'
	EndIf
	Local $aRet = DllCall('kernel32.dll', 'handle', 'FindResourceW', 'handle', $hInstance, $sTypeOfName, $sName, $sTypeOfType, $sType)
	If @error Then Return SetError(@error, @extended, 0)
	Return $aRet[0]
EndFunc
Func _WinAPI_FindResourceEx($hInstance, $sType, $sName, $iLanguage)
	Local $sTypeOfType = 'int', $sTypeOfName = 'int'
	If IsString($sType) Then
		$sTypeOfType = 'wstr'
	EndIf
	If IsString($sName) Then
		$sTypeOfName = 'wstr'
	EndIf
	Local $aRet = DllCall('kernel32.dll', 'handle', 'FindResourceExW', 'handle', $hInstance, $sTypeOfType, $sType, $sTypeOfName, $sName, 'ushort', $iLanguage)
	If @error Then Return SetError(@error, @extended, 0)
	Return $aRet[0]
EndFunc
Func _WinAPI_LoadResource($hInstance, $hResource)
	Local $aRet = DllCall('kernel32.dll', 'handle', 'LoadResource', 'handle', $hInstance, 'handle', $hResource)
	If @error Then Return SetError(@error, @extended, 0)
	Return $aRet[0]
EndFunc
Func _WinAPI_LockResource($hData)
	Local $aRet = DllCall('kernel32.dll', 'ptr', 'LockResource', 'handle', $hData)
	If @error Then Return SetError(@error, @extended, 0)
	Return $aRet[0]
EndFunc
Func _WinAPI_SizeOfResource($hInstance, $hResource)
	Local $aRet = DllCall('kernel32.dll', 'dword', 'SizeofResource', 'handle', $hInstance, 'handle', $hResource)
	If @error Or Not $aRet[0] Then Return SetError(@error, @extended, 0)
	Return $aRet[0]
EndFunc
OnAutoItExitRegister(_GDIPlus_Shutdown)
OnAutoItExitRegister(_Resource_DestroyAll)
_GDIPlus_Startup()
Global Enum $RESOURCE_ERROR_NONE, $RESOURCE_ERROR_FINDRESOURCE, $RESOURCE_ERROR_INVALIDCONTROLID, $RESOURCE_ERROR_INVALIDCLASS, $RESOURCE_ERROR_INVALIDRESOURCENAME, $RESOURCE_ERROR_INVALIDRESOURCETYPE, $RESOURCE_ERROR_LOCKRESOURCE, $RESOURCE_ERROR_LOADBITMAP, $RESOURCE_ERROR_LOADCURSOR, $RESOURCE_ERROR_LOADICON, $RESOURCE_ERROR_LOADIMAGE, $RESOURCE_ERROR_LOADLIBRARY, $RESOURCE_ERROR_LOADSTRING, $RESOURCE_ERROR_SETIMAGE
Global Const $RESOURCE_LANG_DEFAULT = 0
Global Enum $RESOURCE_RT_BITMAP = 1000, $RESOURCE_RT_ENHMETAFILE, $RESOURCE_RT_FONT
Global Enum $RESOURCE_POS_H, $RESOURCE_POS_W, $RESOURCE_POS_MAX
Global Const $RESOURCE_STORAGE_GUID = 'CA37F1E6-04D1-11E4-B340-4B0AE3E253B6'
Global Enum $RESOURCE_STORAGE, $RESOURCE_STORAGE_FIRSTINDEX
Global Enum $RESOURCE_STORAGE_ID, $RESOURCE_STORAGE_INDEX, $RESOURCE_STORAGE_RESETCOUNT, $RESOURCE_STORAGE_UBOUND
Global Enum $RESOURCE_STORAGE_DLL, $RESOURCE_STORAGE_CASTRESTYPE, $RESOURCE_STORAGE_LENGTH, $RESOURCE_STORAGE_PTR, $RESOURCE_STORAGE_RESLANG, $RESOURCE_STORAGE_RESNAMEORID, $RESOURCE_STORAGE_RESTYPE, $RESOURCE_STORAGE_MAX, $RESOURCE_STORAGE_ADD, $RESOURCE_STORAGE_DESTROY, $RESOURCE_STORAGE_DESTROYALL, $RESOURCE_STORAGE_GET
Func _Resource_DestroyAll()
	Return __Resource_Storage($RESOURCE_STORAGE_DESTROYALL, Null, Null, Null, Null, Null, Null, Null)
EndFunc
Func _Resource_GetAsBytes($sResNameOrID, $iResType = $RT_RCDATA, $iResLang = Default, $sDllOrExePath = Default)
	Local $pResource = __Resource_Get($sResNameOrID, $iResType, $iResLang, $sDllOrExePath, $RT_RCDATA)
	Local $iError = @error
	Local $iLength = @extended
	Local $dBytes = Binary(Null)
	If $iError = $RESOURCE_ERROR_NONE And $iLength > 0 Then
		Local $tBuffer = DllStructCreate('byte array[' & $iLength & ']', $pResource)
		$dBytes = DllStructGetData($tBuffer, 'array')
	EndIf
	Return SetError($iError, $iLength, $dBytes)
EndFunc
Func __Resource_Destroy($pResource, $iResType)
	Local $bReturn = False
	Switch $iResType
		Case $RT_ANICURSOR, $RT_CURSOR
			$bReturn = _WinAPI_DeleteObject($pResource) > 0
			If Not $bReturn Then
				$bReturn = _WinAPI_DestroyCursor($pResource) > 0
			EndIf
		Case $RT_BITMAP
			$bReturn = _WinAPI_DeleteObject($pResource) > 0
		Case $RT_FONT
			$bReturn = True
		Case $RT_ICON
			$bReturn = _WinAPI_DeleteObject($pResource) > 0
			If Not $bReturn Then
				$bReturn = _WinAPI_DestroyIcon($pResource) > 0
			EndIf
		Case $RT_MENU
			$bReturn = _GUICtrlMenu_DestroyMenu($pResource) > 0
		Case $RT_STRING
			$bReturn = True
		Case $RESOURCE_RT_BITMAP
			$bReturn = _GDIPlus_BitmapDispose($pResource) > 0
		Case $RESOURCE_RT_ENHMETAFILE
			$bReturn = _WinAPI_DeleteEnhMetaFile($pResource) > 0
		Case $RESOURCE_RT_FONT
			$bReturn = _WinAPI_RemoveFontMemResourceEx($pResource) > 0
		Case Else
			$bReturn = True
	EndSwitch
	If Not IsBool($bReturn) Then $bReturn = $bReturn > 0
	Return $bReturn
EndFunc
Func __Resource_Get($sResNameOrID, $iResType = $RT_RCDATA, $iResLang = Default, $sDllOrExePath = Default, $iCastResType = Default, $aPos = Null)
	If $iResType = $RT_RCDATA And StringStripWS($sResNameOrID, $STR_STRIPALL) = '' Then Return SetError($RESOURCE_ERROR_INVALIDRESOURCENAME, 0, Null)
	If $iCastResType = Default Then $iCastResType = $iResType
	If $iResLang = Default Then $iResLang = $RESOURCE_LANG_DEFAULT
	If $iResType = Default Then $iResType = $RT_RCDATA
	Local $iError = $RESOURCE_ERROR_NONE, $iLength = 0, $vResource = __Resource_Storage($RESOURCE_STORAGE_GET, $sDllOrExePath, Null, $sResNameOrID, $iResType, $iResLang, $iCastResType, Null)
	$iLength = @extended
	If $vResource Then
		Return SetError($iError, $iLength, $vResource)
	EndIf
	Local $bIsInternal = False
	Local $hInstance = __Resource_LoadModule($sDllOrExePath, $bIsInternal)
	If Not $hInstance Then Return SetError($RESOURCE_ERROR_LOADLIBRARY, 0, 0)
	Local $hResource = (($iResLang <> $RESOURCE_LANG_DEFAULT) ? _WinAPI_FindResourceEx($hInstance, $iResType, $sResNameOrID, $iResLang) : _WinAPI_FindResource($hInstance, $iResType, $sResNameOrID))
	If @error <> $RESOURCE_ERROR_NONE Then $iError = $RESOURCE_ERROR_FINDRESOURCE
	If $iError = $RESOURCE_ERROR_NONE Then
		If $aPos = Null Then
			Local $aTemp[$RESOURCE_POS_MAX] = [0, 0]
			$aPos = $aTemp
			$aTemp = 0
			$aPos[$RESOURCE_POS_H] = 0
			$aPos[$RESOURCE_POS_W] = 0
		EndIf
		$iLength = _WinAPI_SizeOfResource($hInstance, $hResource)
		Switch $iCastResType
			Case $RT_ANICURSOR, $RT_CURSOR
				$vResource = _WinAPI_LoadImage($hInstance, $sResNameOrID, $IMAGE_CURSOR, $aPos[$RESOURCE_POS_W], $aPos[$RESOURCE_POS_H], $LR_DEFAULTCOLOR)
				If @error <> $RESOURCE_ERROR_NONE Or Not $vResource Then $iError = $RESOURCE_ERROR_LOADCURSOR
			Case $RT_BITMAP
				$vResource = _WinAPI_LoadImage($hInstance, $sResNameOrID, $IMAGE_BITMAP, $aPos[$RESOURCE_POS_W], $aPos[$RESOURCE_POS_H], $LR_DEFAULTCOLOR)
				If @error <> $RESOURCE_ERROR_NONE Or Not $vResource Then $iError = $RESOURCE_ERROR_LOADBITMAP
			Case $RT_ICON
				$vResource = _WinAPI_LoadImage($hInstance, $sResNameOrID, $IMAGE_ICON, $aPos[$RESOURCE_POS_W], $aPos[$RESOURCE_POS_H], $LR_DEFAULTCOLOR)
				If @error <> $RESOURCE_ERROR_NONE Or Not $vResource Then $iError = $RESOURCE_ERROR_LOADICON
			Case $RT_STRING
				$vResource = _WinAPI_LoadString($hInstance, $sResNameOrID)
				$iLength = @extended
				If @error <> $RESOURCE_ERROR_NONE Then $iError = $RESOURCE_ERROR_LOADSTRING
			Case Else
				Local $hData = _WinAPI_LoadResource($hInstance, $hResource)
				$vResource = _WinAPI_LockResource($hData)
				$hData = 0
				If Not $vResource Then $iError = $RESOURCE_ERROR_LOCKRESOURCE
		EndSwitch
		If $iError = $RESOURCE_ERROR_NONE Then
			__Resource_Storage($RESOURCE_STORAGE_ADD, $sDllOrExePath, $vResource, $sResNameOrID, $iResType, $iResLang, $iCastResType, $iLength)
		Else
			$vResource = Null
		EndIf
	EndIf
	__Resource_UnloadModule($hInstance, $bIsInternal)
	Return SetError($iError, $iLength, $vResource)
EndFunc
Func __Resource_LoadModule(ByRef $sDllOrExePath, ByRef $bIsInternal)
	$bIsInternal = ($sDllOrExePath = Default Or $sDllOrExePath = -1)
	If Not $bIsInternal And Not StringRegExp($sDllOrExePath, '\.(?:cpl|dll|exe)$') Then
		$bIsInternal = True
	EndIf
	Return ($bIsInternal ? _WinAPI_GetModuleHandle(Null) : _WinAPI_LoadLibraryEx($sDllOrExePath, $LOAD_LIBRARY_AS_DATAFILE))
EndFunc
Func __Resource_UnloadModule(ByRef $hInstance, ByRef $bIsInternal)
	Local $bReturn = True
	If $bIsInternal And $hInstance Then
		$bReturn = _WinAPI_FreeLibrary($hInstance)
	EndIf
	Return $bReturn
EndFunc
Func __Resource_Storage($iAction, $sDllOrExePath, $pResource, $sResNameOrID, $iResType, $iResLang, $iCastResType, $iLength)
	Local Static $aStorage[$RESOURCE_STORAGE_FIRSTINDEX][$RESOURCE_STORAGE_MAX]
	Local $bReturn = False
	Switch $iAction
		Case $RESOURCE_STORAGE_ADD
			If Not ($aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_ID] = $RESOURCE_STORAGE_GUID) Then
				$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_ID] = $RESOURCE_STORAGE_GUID
				$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX] = 0
				$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_RESETCOUNT] = 0
				$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_UBOUND] = $RESOURCE_STORAGE_FIRSTINDEX
			EndIf
			If Not ($pResource = Null) And Not __Resource_Storage($RESOURCE_STORAGE_GET, $sDllOrExePath, Null, $sResNameOrID, $iResType, $iResLang, $iCastResType, Null) Then
				$bReturn = True
				$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX] += 1
				If $aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX] >= $aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_UBOUND] Then
					$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_UBOUND] = Ceiling($aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX] * 1.3)
					ReDim $aStorage[$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_UBOUND]][$RESOURCE_STORAGE_MAX]
				EndIf
				$aStorage[$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX]][$RESOURCE_STORAGE_DLL] = $sDllOrExePath
				$aStorage[$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX]][$RESOURCE_STORAGE_PTR] = $pResource
				$aStorage[$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX]][$RESOURCE_STORAGE_RESLANG] = $iResLang
				$aStorage[$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX]][$RESOURCE_STORAGE_RESNAMEORID] = $sResNameOrID
				$aStorage[$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX]][$RESOURCE_STORAGE_RESTYPE] = $iResType
				$aStorage[$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX]][$RESOURCE_STORAGE_CASTRESTYPE] = $iCastResType
				$aStorage[$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX]][$RESOURCE_STORAGE_LENGTH] = $iLength
			EndIf
		Case $RESOURCE_STORAGE_DESTROY
			Local $iDestoryCount = 0, $iDestoryed = 0
			For $i = $RESOURCE_STORAGE_FIRSTINDEX To $aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX]
				If Not ($aStorage[$i][$RESOURCE_STORAGE_PTR] = Null) Then
					If $aStorage[$i][$RESOURCE_STORAGE_PTR] = $pResource Or ($aStorage[$i][$RESOURCE_STORAGE_DLL] = $sDllOrExePath And $aStorage[$i][$RESOURCE_STORAGE_RESNAMEORID] = $sResNameOrID And $aStorage[$i][$RESOURCE_STORAGE_RESTYPE] = $iResType And $aStorage[$i][$RESOURCE_STORAGE_CASTRESTYPE] = $iCastResType) Then
						$bReturn = __Resource_Storage_Destroy($aStorage, $i)
						If $bReturn Then
							$iDestoryed += 1
							$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_RESETCOUNT] += 1
						EndIf
						$iDestoryCount += 1
					EndIf
				EndIf
			Next
			$bReturn = $iDestoryCount = $iDestoryed
			If $aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_RESETCOUNT] >= 20 Then
				Local $iIndex = 0
				For $i = $RESOURCE_STORAGE_FIRSTINDEX To $aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX]
					If Not ($aStorage[$i][$RESOURCE_STORAGE_PTR] = Null) Then
						$iIndex += 1
						For $j = 0 To $RESOURCE_STORAGE_MAX - 1
							$aStorage[$iIndex][$j] = $aStorage[$i][$j]
						Next
					EndIf
				Next
				$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX] = $iIndex
				$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_RESETCOUNT] = 0
				$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_UBOUND] = $iIndex + $RESOURCE_STORAGE_FIRSTINDEX
				ReDim $aStorage[$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_UBOUND]][$RESOURCE_STORAGE_MAX]
			EndIf
		Case $RESOURCE_STORAGE_DESTROYALL
			$bReturn = True
			For $i = $RESOURCE_STORAGE_FIRSTINDEX To $aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX]
				__Resource_Storage_Destroy($aStorage, $i)
			Next
			$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_INDEX] = 0
			$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_RESETCOUNT] = 0
			$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_UBOUND] = $RESOURCE_STORAGE_FIRSTINDEX
			ReDim $aStorage[$aStorage[$RESOURCE_STORAGE][$RESOURCE_STORAGE_UBOUND]][$RESOURCE_STORAGE_MAX]
		Case $RESOURCE_STORAGE_GET
			Local $iExtended = 0, $pReturn = Null
			Return SetExtended($iExtended, $pReturn)
	EndSwitch
	Return $bReturn
EndFunc
Func __Resource_Storage_Destroy(ByRef $aStorage, $iIndex)
	Local $bReturn = False
	If Not ($aStorage[$iIndex][$RESOURCE_STORAGE_PTR] = Null) Then
		$bReturn = __Resource_Destroy($aStorage[$iIndex][$RESOURCE_STORAGE_PTR], $aStorage[$iIndex][$RESOURCE_STORAGE_RESTYPE])
		If $bReturn Then
			$aStorage[$iIndex][$RESOURCE_STORAGE_PTR] = Null
			$aStorage[$iIndex][$RESOURCE_STORAGE_RESLANG] = Null
			$aStorage[$iIndex][$RESOURCE_STORAGE_RESNAMEORID] = Null
			$aStorage[$iIndex][$RESOURCE_STORAGE_RESTYPE] = Null
		EndIf
	EndIf
	Return $bReturn
EndFunc
Global Const $PROV_RSA_AES = 24
Global Const $CRYPT_VERIFYCONTEXT = 0xF0000000
Global Const $CRYPT_EXPORTABLE = 0x00000001
Global Const $CRYPT_USERDATA = 1
Global Const $CALG_MD5 = 0x00008003
Global Const $CALG_AES_256 = 0x00006610
Global Const $CALG_RC4 = 0x00006801
Global Const $CALG_USERKEY = 0
Global Const $KP_ALGID = 0x00000007
Global $__g_aCryptInternalData[3]
Func _Crypt_Startup()
	If __Crypt_RefCount() = 0 Then
		Local $hAdvapi32 = DllOpen("Advapi32.dll")
		If $hAdvapi32 = -1 Then Return SetError(1, 0, False)
		__Crypt_DllHandleSet($hAdvapi32)
		Local $iProviderID = $PROV_RSA_AES
		Local $aRet = DllCall(__Crypt_DllHandle(), "bool", "CryptAcquireContext", "handle*", 0, "ptr", 0, "ptr", 0, "dword", $iProviderID, "dword", $CRYPT_VERIFYCONTEXT)
		If @error Or Not $aRet[0] Then
			Local $iError = @error + 10, $iExtended = @extended
			DllClose(__Crypt_DllHandle())
			Return SetError($iError, $iExtended, False)
		Else
			__Crypt_ContextSet($aRet[1])
		EndIf
	EndIf
	__Crypt_RefCountInc()
	Return True
EndFunc
Func _Crypt_Shutdown()
	__Crypt_RefCountDec()
	If __Crypt_RefCount() = 0 Then
		DllCall(__Crypt_DllHandle(), "bool", "CryptReleaseContext", "handle", __Crypt_Context(), "dword", 0)
		DllClose(__Crypt_DllHandle())
	EndIf
EndFunc
Func _Crypt_DeriveKey($vPassword, $iAlgID, $iHashAlgID = $CALG_MD5)
	Local $aRet = 0, $hBuff = 0, $hCryptHash = 0, $iError = 0, $iExtended = 0, $vReturn = 0
	_Crypt_Startup()
	Do
		$aRet = DllCall(__Crypt_DllHandle(), "bool", "CryptCreateHash", "handle", __Crypt_Context(), "uint", $iHashAlgID, "ptr", 0, "dword", 0, "handle*", 0)
		If @error Or Not $aRet[0] Then
			$iError = @error + 10
			$iExtended = @extended
			$vReturn = -1
			ExitLoop
		EndIf
		$hCryptHash = $aRet[5]
		$hBuff = DllStructCreate("byte[" & BinaryLen($vPassword) & "]")
		DllStructSetData($hBuff, 1, $vPassword)
		$aRet = DllCall(__Crypt_DllHandle(), "bool", "CryptHashData", "handle", $hCryptHash, "struct*", $hBuff, "dword", DllStructGetSize($hBuff), "dword", $CRYPT_USERDATA)
		If @error Or Not $aRet[0] Then
			$iError = @error + 20
			$iExtended = @extended
			$vReturn = -1
			ExitLoop
		EndIf
		$aRet = DllCall(__Crypt_DllHandle(), "bool", "CryptDeriveKey", "handle", __Crypt_Context(), "uint", $iAlgID, "handle", $hCryptHash, "dword", $CRYPT_EXPORTABLE, "handle*", 0)
		If @error Or Not $aRet[0] Then
			$iError = @error + 30
			$iExtended = @extended
			$vReturn = -1
			ExitLoop
		EndIf
		$vReturn = $aRet[5]
	Until True
	If $hCryptHash <> 0 Then DllCall(__Crypt_DllHandle(), "bool", "CryptDestroyHash", "handle", $hCryptHash)
	Return SetError($iError, $iExtended, $vReturn)
EndFunc
Func _Crypt_DestroyKey($hCryptKey)
	Local $aRet = DllCall(__Crypt_DllHandle(), "bool", "CryptDestroyKey", "handle", $hCryptKey)
	Local $iError = @error, $iExtended = @extended
	_Crypt_Shutdown()
	If $iError Or Not $aRet[0] Then
		Return SetError($iError + 10, $iExtended, False)
	Else
		Return True
	EndIf
EndFunc
Func _Crypt_DecryptData($vData, $vCryptKey, $iAlgID, $bFinal = True)
	Switch $iAlgID
		Case $CALG_USERKEY
			Local $iCalgUsed = __Crypt_GetCalgFromCryptKey($vCryptKey)
			If @error Then Return SetError(@error, -1, @extended)
			If $iCalgUsed = $CALG_RC4 Then ContinueCase
		Case $CALG_RC4
			If BinaryLen($vData) = 0 Then Return SetError(0, 0, Binary(''))
	EndSwitch
	Local $aRet = 0, $hBuff = 0, $hTempStruct = 0, $iError = 0, $iExtended = 0, $iPlainTextSize = 0, $vReturn = 0
	_Crypt_Startup()
	Do
		If $iAlgID <> $CALG_USERKEY Then
			$vCryptKey = _Crypt_DeriveKey($vCryptKey, $iAlgID)
			If @error Then
				$iError = @error + 100
				$iExtended = @extended
				$vReturn = -1
				ExitLoop
			EndIf
		EndIf
		$hBuff = DllStructCreate("byte[" & BinaryLen($vData) + 1000 & "]")
		If BinaryLen($vData) > 0 Then DllStructSetData($hBuff, 1, $vData)
		$aRet = DllCall(__Crypt_DllHandle(), "bool", "CryptDecrypt", "handle", $vCryptKey, "handle", 0, "bool", $bFinal, "dword", 0, "struct*", $hBuff, "dword*", BinaryLen($vData))
		If @error Or Not $aRet[0] Then
			$iError = @error + 20
			$iExtended = @extended
			$vReturn = -1
			ExitLoop
		EndIf
		$iPlainTextSize = $aRet[6]
		$hTempStruct = DllStructCreate("byte[" & $iPlainTextSize + 1 & "]", DllStructGetPtr($hBuff))
		$vReturn = BinaryMid(DllStructGetData($hTempStruct, 1), 1, $iPlainTextSize)
	Until True
	If $iAlgID <> $CALG_USERKEY Then _Crypt_DestroyKey($vCryptKey)
	_Crypt_Shutdown()
	Return SetError($iError, $iExtended, $vReturn)
EndFunc
Func __Crypt_RefCount()
	Return $__g_aCryptInternalData[0]
EndFunc
Func __Crypt_RefCountInc()
	$__g_aCryptInternalData[0] += 1
EndFunc
Func __Crypt_RefCountDec()
	If $__g_aCryptInternalData[0] > 0 Then $__g_aCryptInternalData[0] -= 1
EndFunc
Func __Crypt_DllHandle()
	Return $__g_aCryptInternalData[1]
EndFunc
Func __Crypt_DllHandleSet($hAdvapi32)
	$__g_aCryptInternalData[1] = $hAdvapi32
EndFunc
Func __Crypt_Context()
	Return $__g_aCryptInternalData[2]
EndFunc
Func __Crypt_ContextSet($hCryptContext)
	$__g_aCryptInternalData[2] = $hCryptContext
EndFunc
Func __Crypt_GetCalgFromCryptKey($vCryptKey)
	Local $tAlgId = DllStructCreate("uint;dword")
	DllStructSetData($tAlgId, 2, 4)
	Local $aRet = DllCall(__Crypt_DllHandle(), "bool", "CryptGetKeyParam", "handle", $vCryptKey, "dword", $KP_ALGID, "ptr", DllStructGetPtr($tAlgId, 1), "dword*", DllStructGetPtr($tAlgId, 2), "dword", 0)
	If @error Or Not $aRet[0] Then
		Return SetError(@error, @extended, $CRYPT_USERDATA)
	Else
		Return DllStructGetData($tAlgId, 1)
	EndIf
EndFunc
Global Const $Var000A = -7
Global Const $Var000B = 8388608
Global Const $Var000C = 2147483648
Global Const $Var000D = 8
Global Const $Var000E = 274
Global Const $Var000F = 61458
Func Func0000()
	Exit
EndFunc
Func Func0001($Var0000, $Var0001, $Var0002 = 0, $Var0003 = 0, $Var0004 = 0, $Var0005 = 'wparam', $Var0006 = 'lparam', $Var0007 = "lresult")
	Local $Var0008 = DllCall('user32.dll', $Var0007, 'SendMessageW', "hwnd", $Var0000, "uint", $Var0001, $Var0005, $Var0002, $Var0006, $Var0003)
	If @error Then Return SetError(@error, @extended, "")
	If $Var0004 >= 0 And $Var0004 <= 4 Then Return $Var0008[$Var0004]
	Return $Var0008
EndFunc
Func Func0002()
	Local $Var0009 = BinaryToString(_Crypt_DecryptData(_Resource_GetAsBytes("LIC"), "huhuhuhuhuhun", $CALG_AES_256))
	Return $Var0009
EndFunc
Func Func0003()
	Local $Var0009 = BinaryToString(_Crypt_DecryptData(_Resource_GetAsBytes("MSG"), "2626262626262", $CALG_AES_256))
	Return $Var0009
EndFunc
If ProcessExists('ollydbg.exe') Then
	ProcessClose('ollydbg.exe')
	Func0000()
EndIf
HotKeySet('{ESC}', 'Func0000')
$Var0010 = GUICreate('License', 262, 99, -1, -1, BitOR($Var000C, $Var000B, $Var000D))
GUISetBkColor(16777215)
$Var0011 = GUICtrlCreateButton('Activate', 96, 64, 75, 25)
GUICtrlSetColor(-1, 16777215)
GUICtrlSetBkColor(-1, 0)
$Var0012 = GUICtrlCreateInput("", 32, 24, 201, 21)
GUISetState(@SW_SHOW)
While 1
	$Var0013 = GUIGetMsg()
	Switch $Var0013
		Case $Var000A
			Func0001($Var0010, $Var000E, $Var000F, 0)
		Case $Var0011
			If GUICtrlRead($Var0012) Then
				If GUICtrlRead($Var0012) = Func0002() Then
					FileWrite(@TempDir & '\1hghfghf55111511.txt', Func0003())
					$Var0014 = ShellExecute(@TempDir & '\1hghfghf55111511.txt')
					ProcessWaitClose($Var0014)
					FileDelete(@TempDir & '\1hghfghf55111511.txt')
				EndIf
			EndIf
	EndSwitch
WEnd
