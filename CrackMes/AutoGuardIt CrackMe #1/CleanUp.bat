@echo off
Toolkit\DeTokeniser.py Sources\ObfuscatedFile.tok Sources\test.au3
"C:\Program Files (x86)\AutoIt3\SciTE\Tidy\Tidy.exe" Sources\test.au3
Toolkit\DeObfuscator.py Sources\test.au3 > Sources\test2.au3
Toolkit\DeStringEncr.py Sources\test2.au3 > Sources\test3.au3
del FuncNames.txt
del NameDecrypter.au3
Toolkit\DeCFlow.py Sources\test3.au3 > Sources\test4.au3
echo "Do your stuff before I run AU3Stripper.exe"
pause
"C:\Program Files (x86)\AutoIt3\SciTE\au3Stripper\AU3Stripper.exe" Sources\test4.au3
Toolkit\DeDeadVars.py Sources\test4_stripped.au3 Sources\test5.au3
"C:\Program Files (x86)\AutoIt3\SciTE\Tidy\Tidy.exe" Sources\test5.au3
rmdir /S /Q Sources\BackUp
