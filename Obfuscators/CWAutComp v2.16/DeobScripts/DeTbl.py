import binascii, re

TBLFile = open('CWAutComp_v2.16.au3.tbl', 'rb')
TBLList = TBLFile.read().split('t2E0!')
TBLFile.close()

for Elem in xrange(len(TBLList)):
    TBLList[Elem] = '"%s"' % binascii.unhexlify(TBLList[Elem])

ScriptFile = open('CWAutComp_v2.16.au3', 'rb')
ScriptData = ScriptFile.read()
ScriptFile.close()

for Elem in xrange(len(TBLList) - 1):
    Expr = 'DYUFZ\(\$GSUPH\[%d\],\ [0-9]{3}\)' % (Elem + 1)
    ReplStr = re.search(Expr, ScriptData)
    ReplStr = ReplStr.group(0)
    ScriptData = ScriptData.replace(ReplStr, TBLList[Elem])

ScriptFile = open('CWAutComp_v2.16.deobf1.au3', 'wb')
ScriptFile.write(ScriptData)
ScriptFile.close()
