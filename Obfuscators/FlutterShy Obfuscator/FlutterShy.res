Func _Flutter_Crypt($sText,$sPass)
	$sText = BinaryToString($sText)
	$new_Text = _Crypt_DecryptData($sText, $sPass, $CALG_AES_256)
	$new_Text = BinaryToString($new_Text)
	Return $new_Text
EndFunc