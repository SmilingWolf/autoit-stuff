#!/usr/bin/env python2

# Emulates AutoIt3 Random(min, max, 1) and SRandom(seed) functions

import sys, AU3MT

def AU3SRandom(Seed):
    twister.initialize_generator(Seed)
    return 1

def AU3Random(RandMin, RandMax):
    result = twister.extract_number()
    return (result % (RandMax - RandMin + 1)) + RandMin

if __name__ == '__main__':
    print AU3Random(sys.argv[1], sys.argv[2])
