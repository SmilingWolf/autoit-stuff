#!/usr/bin/env python2

import sys, re

Script = open(sys.argv[1], 'r').read()

FullLines = re.findall('StringLen\(".*?"\)', Script, re.MULTILINE)
for Elem in FullLines:
    DeobfVal = '%d' % (len(Elem) - len('StringLen("")'))
    InsVar = re.compile(re.escape(Elem))
    Script = InsVar.sub(DeobfVal, Script)

FullLines = re.findall('Execute\(BinaryToString\(.*\)\)', Script)
ScriptFile = open('NameDecrypter.au3', 'wb')
ScriptFile.write('$hFile = FileOpen(\'FuncNames.txt\', 2)\r\n')
for Elem in FullLines:
    ScriptFile.write('$Assignement = \'%s\'\r\n' % Elem)
    ScriptFile.write('FileWriteLine($hFile, $Assignement & \' = "\' & %s & \'"\')\r\n' % Elem[8:-1])
ScriptFile.write('FileClose($hFile)\r\n')
ScriptFile.close()
# We have to edit NameDecrypter.au3 to add a missing function, then we can manually execute it
# and use the resulting FuncNames.txt
# subprocess.call('"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" NameDecrypter.au3', shell=True)
DeobfAssign = open('FuncNames.txt', 'r').readlines()
for Elem in xrange(len(DeobfAssign)):
    Expr = DeobfAssign[Elem].split(' = ')[0]
    Sub = DeobfAssign[Elem].split(' = ')[1][1:-2]
    Script = Script.replace(Expr, Sub)

FullLines = re.findall('Assign\(\'.*\'\)', Script, re.MULTILINE)
for Elem in FullLines:
    Var = '$%s' % (Elem.split(', ')[0][8:-1])
    Val = Elem.split(', ')[1][:-1]
    Script = Script.replace(Var, Val)
    Script = Script.replace(Var.lower(), Val)

print Script
