#!/usr/bin/env python2

# Incomplete and likely buggy port of
# mATE's DeTokeniser.bas mod by kao
# to Python because f*ck trying to build it
# using VB6 SP6 on my XP SP3 VM

import sys, struct
from AU3Utils import EncDecString

def DataToText(InputData):
    DataIndex = 0
    Lines = struct.unpack('<L', InputData[DataIndex:DataIndex + 4])[0]
    DataIndex += 4
    # print 'Code Lines: %i   0x%08X' % (Lines, Lines)

    ScriptText = ''
    SourceCodeLine = []
    Line = 0
    while Line < Lines:
        cmd = ord(InputData[DataIndex])
        DataIndex += 1
        if cmd == 0x00:
            # keywords
            int32_0 = struct.unpack('<L', InputData[DataIndex:DataIndex + 4])[0]
            DataIndex += 4
            if int32_0 == 0x01:
                SourceCodeLine.append(' AND ')
            elif int32_0 == 0x02:
                SourceCodeLine.append(' OR ')
            elif int32_0 == 0x03:
                SourceCodeLine.append(' NOT ')
            elif int32_0 == 0x04:
                SourceCodeLine.append(' IF ')
            elif int32_0 == 0x05:
                SourceCodeLine.append(' THEN ')
            elif int32_0 == 0x06:
                SourceCodeLine.append(' ELSE ')
            elif int32_0 == 0x07:
                SourceCodeLine.append(' ELSEIF ')
            elif int32_0 == 0x08:
                SourceCodeLine.append(' ENDIF ')
            elif int32_0 == 0x09:
                SourceCodeLine.append(' WHILE ')
            elif int32_0 == 0x0A:
                SourceCodeLine.append(' WEND ')
            elif int32_0 == 0x0B:
                SourceCodeLine.append(' DO ')
            elif int32_0 == 0x0C:
                SourceCodeLine.append(' UNTIL ')
            elif int32_0 == 0x0D:
                SourceCodeLine.append(' FOR ')
            elif int32_0 == 0x0E:
                SourceCodeLine.append(' NEXT ')
            elif int32_0 == 0x0F:
                SourceCodeLine.append(' TO ')
            elif int32_0 == 0x10:
                SourceCodeLine.append(' STEP ')
            elif int32_0 == 0x11:
                SourceCodeLine.append(' IN ')
            elif int32_0 == 0x12:
                SourceCodeLine.append(' EXITLOOP ')
            elif int32_0 == 0x13:
                SourceCodeLine.append(' CONTINUELOOP ')
            elif int32_0 == 0x14:
                SourceCodeLine.append(' SELECT ')
            elif int32_0 == 0x15:
                SourceCodeLine.append(' CASE ')
            elif int32_0 == 0x16:
                SourceCodeLine.append(' ENDSELECT ')
            elif int32_0 == 0x17:
                SourceCodeLine.append(' SWITCH ')
            elif int32_0 == 0x18:
                SourceCodeLine.append(' ENDSWITCH ')
            elif int32_0 == 0x19:
                SourceCodeLine.append(' CONTINUECASE ')
            elif int32_0 == 0x1A:
                SourceCodeLine.append(' DIM ')
            elif int32_0 == 0x1B:
                SourceCodeLine.append(' REDIM ')
            elif int32_0 == 0x1C:
                SourceCodeLine.append(' LOCAL ')
            elif int32_0 == 0x1D:
                SourceCodeLine.append(' GLOBAL ')
            elif int32_0 == 0x1E:
                SourceCodeLine.append(' CONST ')
            elif int32_0 == 0x1F:
                SourceCodeLine.append(' STATIC ')
            elif int32_0 == 0x20:
                SourceCodeLine.append(' FUNC ')
            elif int32_0 == 0x21:
                SourceCodeLine.append(' ENDFUNC ')
            elif int32_0 == 0x22:
                SourceCodeLine.append(' RETURN ')
            elif int32_0 == 0x23:
                SourceCodeLine.append(' EXIT ')
            elif int32_0 == 0x24:
                SourceCodeLine.append(' BYREF ')
            elif int32_0 == 0x25:
                SourceCodeLine.append(' WITH ')
            elif int32_0 == 0x26:
                SourceCodeLine.append(' ENDWITH ')
            elif int32_0 == 0x27:
                SourceCodeLine.append(' TRUE ')
            elif int32_0 == 0x28:
                SourceCodeLine.append(' FALSE ')
            elif int32_0 == 0x29:
                SourceCodeLine.append(' DEFAULT ')
            elif int32_0 == 0x2A:
                SourceCodeLine.append(' NULL ')
            else:
                SourceCodeLine.append(' Unknown Keyword: %02X ' % int32_0)
        elif cmd == 0x01:
            # built-in function calls
            int32_0 = struct.unpack('<L', InputData[DataIndex:DataIndex + 4])[0]
            DataIndex += 4
            if int32_0 == 0x01:
                SourceCodeLine.append('ACOS')
            elif int32_0 == 0x02:
                SourceCodeLine.append('ADLIBREGISTER')
            elif int32_0 == 0x03:
                SourceCodeLine.append('ADLIBUNREGISTER')
            elif int32_0 == 0x04:
                SourceCodeLine.append('ASC')
            elif int32_0 == 0x05:
                SourceCodeLine.append('ASCW')
            elif int32_0 == 0x06:
                SourceCodeLine.append('ASIN')
            elif int32_0 == 0x07:
                SourceCodeLine.append('ASSIGN')
            elif int32_0 == 0x08:
                SourceCodeLine.append('ATAN')
            elif int32_0 == 0x09:
                SourceCodeLine.append('AUTOITSETOPTION')
            elif int32_0 == 0x0A:
                SourceCodeLine.append('AUTOITWINGETTITLE')
            elif int32_0 == 0x0B:
                SourceCodeLine.append('AUTOITWINSETTITLE')
            elif int32_0 == 0x0C:
                SourceCodeLine.append('BEEP')
            elif int32_0 == 0x0D:
                SourceCodeLine.append('BINARY')
            elif int32_0 == 0x0E:
                SourceCodeLine.append('BINARYLEN')
            elif int32_0 == 0x0F:
                SourceCodeLine.append('BINARYMID')
            elif int32_0 == 0x10:
                SourceCodeLine.append('BINARYTOSTRING')
            elif int32_0 == 0x11:
                SourceCodeLine.append('BITAND')
            elif int32_0 == 0x12:
                SourceCodeLine.append('BITNOT')
            elif int32_0 == 0x13:
                SourceCodeLine.append('BITOR')
            elif int32_0 == 0x14:
                SourceCodeLine.append('BITROTATE')
            elif int32_0 == 0x15:
                SourceCodeLine.append('BITSHIFT')
            elif int32_0 == 0x16:
                SourceCodeLine.append('BITXOR')
            elif int32_0 == 0x17:
                SourceCodeLine.append('BLOCKINPUT')
            elif int32_0 == 0x18:
                SourceCodeLine.append('BREAK')
            elif int32_0 == 0x19:
                SourceCodeLine.append('CALL')
            elif int32_0 == 0x1A:
                SourceCodeLine.append('CDTRAY')
            elif int32_0 == 0x1B:
                SourceCodeLine.append('CEILING')
            elif int32_0 == 0x1C:
                SourceCodeLine.append('CHR')
            elif int32_0 == 0x1D:
                SourceCodeLine.append('CHRW')
            elif int32_0 == 0x1E:
                SourceCodeLine.append('CLIPGET')
            elif int32_0 == 0x1F:
                SourceCodeLine.append('CLIPPUT')
            elif int32_0 == 0x20:
                SourceCodeLine.append('CONSOLEREAD')
            elif int32_0 == 0x21:
                SourceCodeLine.append('CONSOLEWRITE')
            elif int32_0 == 0x22:
                SourceCodeLine.append('CONSOLEWRITEERROR')
            elif int32_0 == 0x23:
                SourceCodeLine.append('CONTROLCLICK')
            elif int32_0 == 0x24:
                SourceCodeLine.append('CONTROLCOMMAND')
            elif int32_0 == 0x25:
                SourceCodeLine.append('CONTROLDISABLE')
            elif int32_0 == 0x26:
                SourceCodeLine.append('CONTROLENABLE')
            elif int32_0 == 0x27:
                SourceCodeLine.append('CONTROLFOCUS')
            elif int32_0 == 0x28:
                SourceCodeLine.append('CONTROLGETFOCUS')
            elif int32_0 == 0x29:
                SourceCodeLine.append('CONTROLGETHANDLE')
            elif int32_0 == 0x2A:
                SourceCodeLine.append('CONTROLGETPOS')
            elif int32_0 == 0x2B:
                SourceCodeLine.append('CONTROLGETTEXT')
            elif int32_0 == 0x2C:
                SourceCodeLine.append('CONTROLHIDE')
            elif int32_0 == 0x2D:
                SourceCodeLine.append('CONTROLLISTVIEW')
            elif int32_0 == 0x2E:
                SourceCodeLine.append('CONTROLMOVE')
            elif int32_0 == 0x2F:
                SourceCodeLine.append('CONTROLSEND')
            elif int32_0 == 0x30:
                SourceCodeLine.append('CONTROLSETTEXT')
            elif int32_0 == 0x31:
                SourceCodeLine.append('CONTROLSHOW')
            elif int32_0 == 0x32:
                SourceCodeLine.append('CONTROLTREEVIEW')
            elif int32_0 == 0x33:
                SourceCodeLine.append('COS')
            elif int32_0 == 0x34:
                SourceCodeLine.append('DEC')
            elif int32_0 == 0x35:
                SourceCodeLine.append('DIRCOPY')
            elif int32_0 == 0x36:
                SourceCodeLine.append('DIRCREATE')
            elif int32_0 == 0x37:
                SourceCodeLine.append('DIRGETSIZE')
            elif int32_0 == 0x38:
                SourceCodeLine.append('DIRMOVE')
            elif int32_0 == 0x39:
                SourceCodeLine.append('DIRREMOVE')
            elif int32_0 == 0x3A:
                SourceCodeLine.append('DLLCALL')
            elif int32_0 == 0x3B:
                SourceCodeLine.append('DLLCALLADDRESS')
            elif int32_0 == 0x3C:
                SourceCodeLine.append('DLLCALLBACKFREE')
            elif int32_0 == 0x3D:
                SourceCodeLine.append('DLLCALLBACKGETPTR')
            elif int32_0 == 0x3E:
                SourceCodeLine.append('DLLCALLBACKREGISTER')
            elif int32_0 == 0x3F:
                SourceCodeLine.append('DLLCLOSE')
            elif int32_0 == 0x40:
                SourceCodeLine.append('DLLOPEN')
            elif int32_0 == 0x41:
                SourceCodeLine.append('DLLSTRUCTCREATE')
            elif int32_0 == 0x42:
                SourceCodeLine.append('DLLSTRUCTGETDATA')
            elif int32_0 == 0x43:
                SourceCodeLine.append('DLLSTRUCTGETPTR')
            elif int32_0 == 0x44:
                SourceCodeLine.append('DLLSTRUCTGETSIZE')
            elif int32_0 == 0x45:
                SourceCodeLine.append('DLLSTRUCTSETDATA')
            elif int32_0 == 0x46:
                SourceCodeLine.append('DRIVEGETDRIVE')
            elif int32_0 == 0x47:
                SourceCodeLine.append('DRIVEGETFILESYSTEM')
            elif int32_0 == 0x48:
                SourceCodeLine.append('DRIVEGETLABEL')
            elif int32_0 == 0x49:
                SourceCodeLine.append('DRIVEGETSERIAL')
            elif int32_0 == 0x4A:
                SourceCodeLine.append('DRIVEGETTYPE')
            elif int32_0 == 0x4B:
                SourceCodeLine.append('DRIVEMAPADD')
            elif int32_0 == 0x4C:
                SourceCodeLine.append('DRIVEMAPDEL')
            elif int32_0 == 0x4D:
                SourceCodeLine.append('DRIVEMAPGET')
            elif int32_0 == 0x4E:
                SourceCodeLine.append('DRIVESETLABEL')
            elif int32_0 == 0x4F:
                SourceCodeLine.append('DRIVESPACEFREE')
            elif int32_0 == 0x50:
                SourceCodeLine.append('DRIVESPACETOTAL')
            elif int32_0 == 0x51:
                SourceCodeLine.append('DRIVESTATUS')
            elif int32_0 == 0x52:
                SourceCodeLine.append('DUMMYSPEEDTEST')
            elif int32_0 == 0x53:
                SourceCodeLine.append('ENVGET')
            elif int32_0 == 0x54:
                SourceCodeLine.append('ENVSET')
            elif int32_0 == 0x55:
                SourceCodeLine.append('ENVUPDATE')
            elif int32_0 == 0x56:
                SourceCodeLine.append('EVAL')
            elif int32_0 == 0x57:
                SourceCodeLine.append('EXECUTE')
            elif int32_0 == 0x58:
                SourceCodeLine.append('EXP')
            elif int32_0 == 0x59:
                SourceCodeLine.append('FILECHANGEDIR')
            elif int32_0 == 0x5A:
                SourceCodeLine.append('FILECLOSE')
            elif int32_0 == 0x5B:
                SourceCodeLine.append('FILECOPY')
            elif int32_0 == 0x5C:
                SourceCodeLine.append('FILECREATENTFSLINK')
            elif int32_0 == 0x5D:
                SourceCodeLine.append('FILECREATESHORTCUT')
            elif int32_0 == 0x5E:
                SourceCodeLine.append('FILEDELETE')
            elif int32_0 == 0x5F:
                SourceCodeLine.append('FILEEXISTS')
            elif int32_0 == 0x60:
                SourceCodeLine.append('FILEFINDFIRSTFILE')
            elif int32_0 == 0x61:
                SourceCodeLine.append('FILEFINDNEXTFILE')
            elif int32_0 == 0x62:
                SourceCodeLine.append('FILEFLUSH')
            elif int32_0 == 0x63:
                SourceCodeLine.append('FILEGETATTRIB')
            elif int32_0 == 0x64:
                SourceCodeLine.append('FILEGETENCODING')
            elif int32_0 == 0x65:
                SourceCodeLine.append('FILEGETLONGNAME')
            elif int32_0 == 0x66:
                SourceCodeLine.append('FILEGETPOS')
            elif int32_0 == 0x67:
                SourceCodeLine.append('FILEGETSHORTCUT')
            elif int32_0 == 0x68:
                SourceCodeLine.append('FILEGETSHORTNAME')
            elif int32_0 == 0x69:
                SourceCodeLine.append('FILEGETSIZE')
            elif int32_0 == 0x6A:
                SourceCodeLine.append('FILEGETTIME')
            elif int32_0 == 0x6B:
                SourceCodeLine.append('FILEGETVERSION')
            elif int32_0 == 0x6C:
                SourceCodeLine.append('FILEINSTALL')
            elif int32_0 == 0x6D:
                SourceCodeLine.append('FILEMOVE')
            elif int32_0 == 0x6E:
                SourceCodeLine.append('FILEOPEN')
            elif int32_0 == 0x6F:
                SourceCodeLine.append('FILEOPENDIALOG')
            elif int32_0 == 0x70:
                SourceCodeLine.append('FILEREAD')
            elif int32_0 == 0x71:
                SourceCodeLine.append('FILEREADLINE')
            elif int32_0 == 0x72:
                SourceCodeLine.append('FILEREADTOARRAY')
            elif int32_0 == 0x73:
                SourceCodeLine.append('FILERECYCLE')
            elif int32_0 == 0x74:
                SourceCodeLine.append('FILERECYCLEEMPTY')
            elif int32_0 == 0x75:
                SourceCodeLine.append('FILESAVEDIALOG')
            elif int32_0 == 0x76:
                SourceCodeLine.append('FILESELECTFOLDER')
            elif int32_0 == 0x77:
                SourceCodeLine.append('FILESETATTRIB')
            elif int32_0 == 0x78:
                SourceCodeLine.append('FILESETEND')
            elif int32_0 == 0x79:
                SourceCodeLine.append('FILESETPOS')
            elif int32_0 == 0x7A:
                SourceCodeLine.append('FILESETTIME')
            elif int32_0 == 0x7B:
                SourceCodeLine.append('FILEWRITE')
            elif int32_0 == 0x7C:
                SourceCodeLine.append('FILEWRITELINE')
            elif int32_0 == 0x7D:
                SourceCodeLine.append('FLOOR')
            elif int32_0 == 0x7E:
                SourceCodeLine.append('FTPSETPROXY')
            elif int32_0 == 0x7F:
                SourceCodeLine.append('FUNCNAME')
            elif int32_0 == 0x80:
                SourceCodeLine.append('GUICREATE')
            elif int32_0 == 0x81:
                SourceCodeLine.append('GUICTRLCREATEAVI')
            elif int32_0 == 0x82:
                SourceCodeLine.append('GUICTRLCREATEBUTTON')
            elif int32_0 == 0x83:
                SourceCodeLine.append('GUICTRLCREATECHECKBOX')
            elif int32_0 == 0x84:
                SourceCodeLine.append('GUICTRLCREATECOMBO')
            elif int32_0 == 0x85:
                SourceCodeLine.append('GUICTRLCREATECONTEXTMENU')
            elif int32_0 == 0x86:
                SourceCodeLine.append('GUICTRLCREATEDATE')
            elif int32_0 == 0x87:
                SourceCodeLine.append('GUICTRLCREATEDUMMY')
            elif int32_0 == 0x88:
                SourceCodeLine.append('GUICTRLCREATEEDIT')
            elif int32_0 == 0x89:
                SourceCodeLine.append('GUICTRLCREATEGRAPHIC')
            elif int32_0 == 0x8A:
                SourceCodeLine.append('GUICTRLCREATEGROUP')
            elif int32_0 == 0x8B:
                SourceCodeLine.append('GUICTRLCREATEICON')
            elif int32_0 == 0x8C:
                SourceCodeLine.append('GUICTRLCREATEINPUT')
            elif int32_0 == 0x8D:
                SourceCodeLine.append('GUICTRLCREATELABEL')
            elif int32_0 == 0x8E:
                SourceCodeLine.append('GUICTRLCREATELIST')
            elif int32_0 == 0x8F:
                SourceCodeLine.append('GUICTRLCREATELISTVIEW')
            elif int32_0 == 0x90:
                SourceCodeLine.append('GUICTRLCREATELISTVIEWITEM')
            elif int32_0 == 0x91:
                SourceCodeLine.append('GUICTRLCREATEMENU')
            elif int32_0 == 0x92:
                SourceCodeLine.append('GUICTRLCREATEMENUITEM')
            elif int32_0 == 0x93:
                SourceCodeLine.append('GUICTRLCREATEMONTHCAL')
            elif int32_0 == 0x94:
                SourceCodeLine.append('GUICTRLCREATEOBJ')
            elif int32_0 == 0x95:
                SourceCodeLine.append('GUICTRLCREATEPIC')
            elif int32_0 == 0x96:
                SourceCodeLine.append('GUICTRLCREATEPROGRESS')
            elif int32_0 == 0x97:
                SourceCodeLine.append('GUICTRLCREATERADIO')
            elif int32_0 == 0x98:
                SourceCodeLine.append('GUICTRLCREATESLIDER')
            elif int32_0 == 0x99:
                SourceCodeLine.append('GUICTRLCREATETAB')
            elif int32_0 == 0x9A:
                SourceCodeLine.append('GUICTRLCREATETABITEM')
            elif int32_0 == 0x9B:
                SourceCodeLine.append('GUICTRLCREATETREEVIEW')
            elif int32_0 == 0x9C:
                SourceCodeLine.append('GUICTRLCREATETREEVIEWITEM')
            elif int32_0 == 0x9D:
                SourceCodeLine.append('GUICTRLCREATEUPDOWN')
            elif int32_0 == 0x9E:
                SourceCodeLine.append('GUICTRLDELETE')
            elif int32_0 == 0x9F:
                SourceCodeLine.append('GUICTRLGETHANDLE')
            elif int32_0 == 0xA0:
                SourceCodeLine.append('GUICTRLGETSTATE')
            elif int32_0 == 0xA1:
                SourceCodeLine.append('GUICTRLREAD')
            elif int32_0 == 0xA2:
                SourceCodeLine.append('GUICTRLRECVMSG')
            elif int32_0 == 0xA3:
                SourceCodeLine.append('GUICTRLREGISTERLISTVIEWSORT')
            elif int32_0 == 0xA4:
                SourceCodeLine.append('GUICTRLSENDMSG')
            elif int32_0 == 0xA5:
                SourceCodeLine.append('GUICTRLSENDTODUMMY')
            elif int32_0 == 0xA6:
                SourceCodeLine.append('GUICTRLSETBKCOLOR')
            elif int32_0 == 0xA7:
                SourceCodeLine.append('GUICTRLSETCOLOR')
            elif int32_0 == 0xA8:
                SourceCodeLine.append('GUICTRLSETCURSOR')
            elif int32_0 == 0xA9:
                SourceCodeLine.append('GUICTRLSETDATA')
            elif int32_0 == 0xAA:
                SourceCodeLine.append('GUICTRLSETDEFBKCOLOR')
            elif int32_0 == 0xAB:
                SourceCodeLine.append('GUICTRLSETDEFCOLOR')
            elif int32_0 == 0xAC:
                SourceCodeLine.append('GUICTRLSETFONT')
            elif int32_0 == 0xAD:
                SourceCodeLine.append('GUICTRLSETGRAPHIC')
            elif int32_0 == 0xAE:
                SourceCodeLine.append('GUICTRLSETIMAGE')
            elif int32_0 == 0xAF:
                SourceCodeLine.append('GUICTRLSETLIMIT')
            elif int32_0 == 0xB0:
                SourceCodeLine.append('GUICTRLSETONEVENT')
            elif int32_0 == 0xB1:
                SourceCodeLine.append('GUICTRLSETPOS')
            elif int32_0 == 0xB2:
                SourceCodeLine.append('GUICTRLSETRESIZING')
            elif int32_0 == 0xB3:
                SourceCodeLine.append('GUICTRLSETSTATE')
            elif int32_0 == 0xB4:
                SourceCodeLine.append('GUICTRLSETSTYLE')
            elif int32_0 == 0xB5:
                SourceCodeLine.append('GUICTRLSETTIP')
            elif int32_0 == 0xB6:
                SourceCodeLine.append('GUIDELETE')
            elif int32_0 == 0xB7:
                SourceCodeLine.append('GUIGETCURSORINFO')
            elif int32_0 == 0xB8:
                SourceCodeLine.append('GUIGETMSG')
            elif int32_0 == 0xB9:
                SourceCodeLine.append('GUIGETSTYLE')
            elif int32_0 == 0xBA:
                SourceCodeLine.append('GUIREGISTERMSG')
            elif int32_0 == 0xBB:
                SourceCodeLine.append('GUISETACCELERATORS')
            elif int32_0 == 0xBC:
                SourceCodeLine.append('GUISETBKCOLOR')
            elif int32_0 == 0xBD:
                SourceCodeLine.append('GUISETCOORD')
            elif int32_0 == 0xBE:
                SourceCodeLine.append('GUISETCURSOR')
            elif int32_0 == 0xBF:
                SourceCodeLine.append('GUISETFONT')
            elif int32_0 == 0xC0:
                SourceCodeLine.append('GUISETHELP')
            elif int32_0 == 0xC1:
                SourceCodeLine.append('GUISETICON')
            elif int32_0 == 0xC2:
                SourceCodeLine.append('GUISETONEVENT')
            elif int32_0 == 0xC3:
                SourceCodeLine.append('GUISETSTATE')
            elif int32_0 == 0xC4:
                SourceCodeLine.append('GUISETSTYLE')
            elif int32_0 == 0xC5:
                SourceCodeLine.append('GUISTARTGROUP')
            elif int32_0 == 0xC6:
                SourceCodeLine.append('GUISWITCH')
            elif int32_0 == 0xC7:
                SourceCodeLine.append('HEX')
            elif int32_0 == 0xC8:
                SourceCodeLine.append('HOTKEYSET')
            elif int32_0 == 0xC9:
                SourceCodeLine.append('HTTPSETPROXY')
            elif int32_0 == 0xCA:
                SourceCodeLine.append('HTTPSETUSERAGENT')
            elif int32_0 == 0xCB:
                SourceCodeLine.append('HWND')
            elif int32_0 == 0xCC:
                SourceCodeLine.append('INETCLOSE')
            elif int32_0 == 0xCD:
                SourceCodeLine.append('INETGET')
            elif int32_0 == 0xCE:
                SourceCodeLine.append('INETGETINFO')
            elif int32_0 == 0xCF:
                SourceCodeLine.append('INETGETSIZE')
            elif int32_0 == 0xD0:
                SourceCodeLine.append('INETREAD')
            elif int32_0 == 0xD1:
                SourceCodeLine.append('INIDELETE')
            elif int32_0 == 0xD2:
                SourceCodeLine.append('INIREAD')
            elif int32_0 == 0xD3:
                SourceCodeLine.append('INIREADSECTION')
            elif int32_0 == 0xD4:
                SourceCodeLine.append('INIREADSECTIONNAMES')
            elif int32_0 == 0xD5:
                SourceCodeLine.append('INIRENAMESECTION')
            elif int32_0 == 0xD6:
                SourceCodeLine.append('INIWRITE')
            elif int32_0 == 0xD7:
                SourceCodeLine.append('INIWRITESECTION')
            elif int32_0 == 0xD8:
                SourceCodeLine.append('INPUTBOX')
            elif int32_0 == 0xD9:
                SourceCodeLine.append('INT')
            elif int32_0 == 0xDA:
                SourceCodeLine.append('ISADMIN')
            elif int32_0 == 0xDB:
                SourceCodeLine.append('ISARRAY')
            elif int32_0 == 0xDC:
                SourceCodeLine.append('ISBINARY')
            elif int32_0 == 0xDD:
                SourceCodeLine.append('ISBOOL')
            elif int32_0 == 0xDE:
                SourceCodeLine.append('ISDECLARED')
            elif int32_0 == 0xDF:
                SourceCodeLine.append('ISDLLSTRUCT')
            elif int32_0 == 0xE0:
                SourceCodeLine.append('ISFLOAT')
            elif int32_0 == 0xE1:
                SourceCodeLine.append('ISFUNC')
            elif int32_0 == 0xE2:
                SourceCodeLine.append('ISHWND')
            elif int32_0 == 0xE3:
                SourceCodeLine.append('ISINT')
            elif int32_0 == 0xE4:
                SourceCodeLine.append('ISKEYWORD')
            elif int32_0 == 0xE5:
                SourceCodeLine.append('ISMAP')
            elif int32_0 == 0xE6:
                SourceCodeLine.append('ISNUMBER')
            elif int32_0 == 0xE7:
                SourceCodeLine.append('ISOBJ')
            elif int32_0 == 0xE8:
                SourceCodeLine.append('ISPTR')
            elif int32_0 == 0xE9:
                SourceCodeLine.append('ISSTRING')
            elif int32_0 == 0xEA:
                SourceCodeLine.append('LOG')
            elif int32_0 == 0xEB:
                SourceCodeLine.append('MAPAPPEND')
            elif int32_0 == 0xEC:
                SourceCodeLine.append('MAPEXISTS')
            elif int32_0 == 0xED:
                SourceCodeLine.append('MAPKEYS')
            elif int32_0 == 0xEE:
                SourceCodeLine.append('MAPREMOVE')
            elif int32_0 == 0xEF:
                SourceCodeLine.append('MEMGETSTATS')
            elif int32_0 == 0xF0:
                SourceCodeLine.append('MOD')
            elif int32_0 == 0xF1:
                SourceCodeLine.append('MOUSECLICK')
            elif int32_0 == 0xF2:
                SourceCodeLine.append('MOUSECLICKDRAG')
            elif int32_0 == 0xF3:
                SourceCodeLine.append('MOUSEDOWN')
            elif int32_0 == 0xF4:
                SourceCodeLine.append('MOUSEGETCURSOR')
            elif int32_0 == 0xF5:
                SourceCodeLine.append('MOUSEGETPOS')
            elif int32_0 == 0xF6:
                SourceCodeLine.append('MOUSEMOVE')
            elif int32_0 == 0xF7:
                SourceCodeLine.append('MOUSEUP')
            elif int32_0 == 0xF8:
                SourceCodeLine.append('MOUSEWHEEL')
            elif int32_0 == 0xF9:
                SourceCodeLine.append('MSGBOX')
            elif int32_0 == 0xFA:
                SourceCodeLine.append('NUMBER')
            elif int32_0 == 0xFB:
                SourceCodeLine.append('OBJCREATE')
            elif int32_0 == 0xFC:
                SourceCodeLine.append('OBJCREATEINTERFACE')
            elif int32_0 == 0xFD:
                SourceCodeLine.append('OBJEVENT')
            elif int32_0 == 0xFE:
                SourceCodeLine.append('OBJGET')
            elif int32_0 == 0xFF:
                SourceCodeLine.append('OBJNAME')
            elif int32_0 == 0x100:
                SourceCodeLine.append('ONAUTOITEXITREGISTER')
            elif int32_0 == 0x101:
                SourceCodeLine.append('ONAUTOITEXITUNREGISTER')
            elif int32_0 == 0x102:
                SourceCodeLine.append('OPT')
            elif int32_0 == 0x103:
                SourceCodeLine.append('PING')
            elif int32_0 == 0x104:
                SourceCodeLine.append('PIXELCHECKSUM')
            elif int32_0 == 0x105:
                SourceCodeLine.append('PIXELGETCOLOR')
            elif int32_0 == 0x106:
                SourceCodeLine.append('PIXELSEARCH')
            elif int32_0 == 0x107:
                SourceCodeLine.append('PROCESSCLOSE')
            elif int32_0 == 0x108:
                SourceCodeLine.append('PROCESSEXISTS')
            elif int32_0 == 0x109:
                SourceCodeLine.append('PROCESSGETSTATS')
            elif int32_0 == 0x10A:
                SourceCodeLine.append('PROCESSLIST')
            elif int32_0 == 0x10B:
                SourceCodeLine.append('PROCESSSETPRIORITY')
            elif int32_0 == 0x10C:
                SourceCodeLine.append('PROCESSWAIT')
            elif int32_0 == 0x10D:
                SourceCodeLine.append('PROCESSWAITCLOSE')
            elif int32_0 == 0x10E:
                SourceCodeLine.append('PROGRESSOFF')
            elif int32_0 == 0x10F:
                SourceCodeLine.append('PROGRESSON')
            elif int32_0 == 0x110:
                SourceCodeLine.append('PROGRESSSET')
            elif int32_0 == 0x111:
                SourceCodeLine.append('PTR')
            elif int32_0 == 0x112:
                SourceCodeLine.append('RANDOM')
            elif int32_0 == 0x113:
                SourceCodeLine.append('REGDELETE')
            elif int32_0 == 0x114:
                SourceCodeLine.append('REGENUMKEY')
            elif int32_0 == 0x115:
                SourceCodeLine.append('REGENUMVAL')
            elif int32_0 == 0x116:
                SourceCodeLine.append('REGREAD')
            elif int32_0 == 0x117:
                SourceCodeLine.append('REGWRITE')
            elif int32_0 == 0x118:
                SourceCodeLine.append('ROUND')
            elif int32_0 == 0x119:
                SourceCodeLine.append('RUN')
            elif int32_0 == 0x11A:
                SourceCodeLine.append('RUNAS')
            elif int32_0 == 0x11B:
                SourceCodeLine.append('RUNASWAIT')
            elif int32_0 == 0x11C:
                SourceCodeLine.append('RUNWAIT')
            elif int32_0 == 0x11D:
                SourceCodeLine.append('SEND')
            elif int32_0 == 0x11E:
                SourceCodeLine.append('SENDKEEPACTIVE')
            elif int32_0 == 0x11F:
                SourceCodeLine.append('SETERROR')
            elif int32_0 == 0x120:
                SourceCodeLine.append('SETEXTENDED')
            elif int32_0 == 0x121:
                SourceCodeLine.append('SHELLEXECUTE')
            elif int32_0 == 0x122:
                SourceCodeLine.append('SHELLEXECUTEWAIT')
            elif int32_0 == 0x123:
                SourceCodeLine.append('SHUTDOWN')
            elif int32_0 == 0x124:
                SourceCodeLine.append('SIN')
            elif int32_0 == 0x125:
                SourceCodeLine.append('SLEEP')
            elif int32_0 == 0x126:
                SourceCodeLine.append('SOUNDPLAY')
            elif int32_0 == 0x127:
                SourceCodeLine.append('SOUNDSETWAVEVOLUME')
            elif int32_0 == 0x128:
                SourceCodeLine.append('SPLASHIMAGEON')
            elif int32_0 == 0x129:
                SourceCodeLine.append('SPLASHOFF')
            elif int32_0 == 0x12A:
                SourceCodeLine.append('SPLASHTEXTON')
            elif int32_0 == 0x12B:
                SourceCodeLine.append('SQRT')
            elif int32_0 == 0x12C:
                SourceCodeLine.append('SRANDOM')
            elif int32_0 == 0x12D:
                SourceCodeLine.append('STATUSBARGETTEXT')
            elif int32_0 == 0x12E:
                SourceCodeLine.append('STDERRREAD')
            elif int32_0 == 0x12F:
                SourceCodeLine.append('STDINWRITE')
            elif int32_0 == 0x130:
                SourceCodeLine.append('STDIOCLOSE')
            elif int32_0 == 0x131:
                SourceCodeLine.append('STDOUTREAD')
            elif int32_0 == 0x132:
                SourceCodeLine.append('STRING')
            elif int32_0 == 0x133:
                SourceCodeLine.append('STRINGADDCR')
            elif int32_0 == 0x134:
                SourceCodeLine.append('STRINGCOMPARE')
            elif int32_0 == 0x135:
                SourceCodeLine.append('STRINGFORMAT')
            elif int32_0 == 0x136:
                SourceCodeLine.append('STRINGFROMASCIIARRAY')
            elif int32_0 == 0x137:
                SourceCodeLine.append('STRINGINSTR')
            elif int32_0 == 0x138:
                SourceCodeLine.append('STRINGISALNUM')
            elif int32_0 == 0x139:
                SourceCodeLine.append('STRINGISALPHA')
            elif int32_0 == 0x13A:
                SourceCodeLine.append('STRINGISASCII')
            elif int32_0 == 0x13B:
                SourceCodeLine.append('STRINGISDIGIT')
            elif int32_0 == 0x13C:
                SourceCodeLine.append('STRINGISFLOAT')
            elif int32_0 == 0x13D:
                SourceCodeLine.append('STRINGISINT')
            elif int32_0 == 0x13E:
                SourceCodeLine.append('STRINGISLOWER')
            elif int32_0 == 0x13F:
                SourceCodeLine.append('STRINGISSPACE')
            elif int32_0 == 0x140:
                SourceCodeLine.append('STRINGISUPPER')
            elif int32_0 == 0x141:
                SourceCodeLine.append('STRINGISXDIGIT')
            elif int32_0 == 0x142:
                SourceCodeLine.append('STRINGLEFT')
            elif int32_0 == 0x143:
                SourceCodeLine.append('STRINGLEN')
            elif int32_0 == 0x144:
                SourceCodeLine.append('STRINGLOWER')
            elif int32_0 == 0x145:
                SourceCodeLine.append('STRINGMID')
            elif int32_0 == 0x146:
                SourceCodeLine.append('STRINGREGEXP')
            elif int32_0 == 0x147:
                SourceCodeLine.append('STRINGREGEXPREPLACE')
            elif int32_0 == 0x148:
                SourceCodeLine.append('STRINGREPLACE')
            elif int32_0 == 0x149:
                SourceCodeLine.append('STRINGREVERSE')
            elif int32_0 == 0x14A:
                SourceCodeLine.append('STRINGRIGHT')
            elif int32_0 == 0x14B:
                SourceCodeLine.append('STRINGSPLIT')
            elif int32_0 == 0x14C:
                SourceCodeLine.append('STRINGSTRIPCR')
            elif int32_0 == 0x14D:
                SourceCodeLine.append('STRINGSTRIPWS')
            elif int32_0 == 0x14E:
                SourceCodeLine.append('STRINGTOASCIIARRAY')
            elif int32_0 == 0x14F:
                SourceCodeLine.append('STRINGTOBINARY')
            elif int32_0 == 0x150:
                SourceCodeLine.append('STRINGTRIMLEFT')
            elif int32_0 == 0x151:
                SourceCodeLine.append('STRINGTRIMRIGHT')
            elif int32_0 == 0x152:
                SourceCodeLine.append('STRINGUPPER')
            elif int32_0 == 0x153:
                SourceCodeLine.append('TAN')
            elif int32_0 == 0x154:
                SourceCodeLine.append('TCPACCEPT')
            elif int32_0 == 0x155:
                SourceCodeLine.append('TCPCLOSESOCKET')
            elif int32_0 == 0x156:
                SourceCodeLine.append('TCPCONNECT')
            elif int32_0 == 0x157:
                SourceCodeLine.append('TCPLISTEN')
            elif int32_0 == 0x158:
                SourceCodeLine.append('TCPNAMETOIP')
            elif int32_0 == 0x159:
                SourceCodeLine.append('TCPRECV')
            elif int32_0 == 0x15A:
                SourceCodeLine.append('TCPSEND')
            elif int32_0 == 0x15B:
                SourceCodeLine.append('TCPSHUTDOWN')
            elif int32_0 == 0x15C:
                SourceCodeLine.append('TCPSTARTUP')
            elif int32_0 == 0x15D:
                SourceCodeLine.append('TIMERDIFF')
            elif int32_0 == 0x15E:
                SourceCodeLine.append('TIMERINIT')
            elif int32_0 == 0x15F:
                SourceCodeLine.append('TOOLTIP')
            elif int32_0 == 0x160:
                SourceCodeLine.append('TRAYCREATEITEM')
            elif int32_0 == 0x161:
                SourceCodeLine.append('TRAYCREATEMENU')
            elif int32_0 == 0x162:
                SourceCodeLine.append('TRAYGETMSG')
            elif int32_0 == 0x163:
                SourceCodeLine.append('TRAYITEMDELETE')
            elif int32_0 == 0x164:
                SourceCodeLine.append('TRAYITEMGETHANDLE')
            elif int32_0 == 0x165:
                SourceCodeLine.append('TRAYITEMGETSTATE')
            elif int32_0 == 0x166:
                SourceCodeLine.append('TRAYITEMGETTEXT')
            elif int32_0 == 0x167:
                SourceCodeLine.append('TRAYITEMSETONEVENT')
            elif int32_0 == 0x168:
                SourceCodeLine.append('TRAYITEMSETSTATE')
            elif int32_0 == 0x169:
                SourceCodeLine.append('TRAYITEMSETTEXT')
            elif int32_0 == 0x16A:
                SourceCodeLine.append('TRAYSETCLICK')
            elif int32_0 == 0x16B:
                SourceCodeLine.append('TRAYSETICON')
            elif int32_0 == 0x16C:
                SourceCodeLine.append('TRAYSETONEVENT')
            elif int32_0 == 0x16D:
                SourceCodeLine.append('TRAYSETPAUSEICON')
            elif int32_0 == 0x16E:
                SourceCodeLine.append('TRAYSETSTATE')
            elif int32_0 == 0x16F:
                SourceCodeLine.append('TRAYSETTOOLTIP')
            elif int32_0 == 0x170:
                SourceCodeLine.append('TRAYTIP')
            elif int32_0 == 0x171:
                SourceCodeLine.append('UBOUND')
            elif int32_0 == 0x172:
                SourceCodeLine.append('UDPBIND')
            elif int32_0 == 0x173:
                SourceCodeLine.append('UDPCLOSESOCKET')
            elif int32_0 == 0x174:
                SourceCodeLine.append('UDPOPEN')
            elif int32_0 == 0x175:
                SourceCodeLine.append('UDPRECV')
            elif int32_0 == 0x176:
                SourceCodeLine.append('UDPSEND')
            elif int32_0 == 0x177:
                SourceCodeLine.append('UDPSHUTDOWN')
            elif int32_0 == 0x178:
                SourceCodeLine.append('UDPSTARTUP')
            elif int32_0 == 0x179:
                SourceCodeLine.append('VARGETTYPE')
            elif int32_0 == 0x17A:
                SourceCodeLine.append('WINACTIVATE')
            elif int32_0 == 0x17B:
                SourceCodeLine.append('WINACTIVE')
            elif int32_0 == 0x17C:
                SourceCodeLine.append('WINCLOSE')
            elif int32_0 == 0x17D:
                SourceCodeLine.append('WINEXISTS')
            elif int32_0 == 0x17E:
                SourceCodeLine.append('WINFLASH')
            elif int32_0 == 0x17F:
                SourceCodeLine.append('WINGETCARETPOS')
            elif int32_0 == 0x180:
                SourceCodeLine.append('WINGETCLASSLIST')
            elif int32_0 == 0x181:
                SourceCodeLine.append('WINGETCLIENTSIZE')
            elif int32_0 == 0x182:
                SourceCodeLine.append('WINGETHANDLE')
            elif int32_0 == 0x183:
                SourceCodeLine.append('WINGETPOS')
            elif int32_0 == 0x184:
                SourceCodeLine.append('WINGETPROCESS')
            elif int32_0 == 0x185:
                SourceCodeLine.append('WINGETSTATE')
            elif int32_0 == 0x186:
                SourceCodeLine.append('WINGETTEXT')
            elif int32_0 == 0x187:
                SourceCodeLine.append('WINGETTITLE')
            elif int32_0 == 0x188:
                SourceCodeLine.append('WINKILL')
            elif int32_0 == 0x189:
                SourceCodeLine.append('WINLIST')
            elif int32_0 == 0x18A:
                SourceCodeLine.append('WINMENUSELECTITEM')
            elif int32_0 == 0x18B:
                SourceCodeLine.append('WINMINIMIZEALL')
            elif int32_0 == 0x18C:
                SourceCodeLine.append('WINMINIMIZEALLUNDO')
            elif int32_0 == 0x18D:
                SourceCodeLine.append('WINMOVE')
            elif int32_0 == 0x18E:
                SourceCodeLine.append('WINSETONTOP')
            elif int32_0 == 0x18F:
                SourceCodeLine.append('WINSETSTATE')
            elif int32_0 == 0x190:
                SourceCodeLine.append('WINSETTITLE')
            elif int32_0 == 0x191:
                SourceCodeLine.append('WINSETTRANS')
            elif int32_0 == 0x192:
                SourceCodeLine.append('WINWAIT')
            elif int32_0 == 0x193:
                SourceCodeLine.append('WINWAITACTIVE')
            elif int32_0 == 0x194:
                SourceCodeLine.append('WINWAITCLOSE')
            elif int32_0 == 0x195:
                SourceCodeLine.append('WINWAITNOTACTIVE')
            else:
                SourceCodeLine.append('Unknown built-in function call: %02X' % int32_0)
        elif cmd == 0x05:
            Number = struct.unpack('<l', InputData[DataIndex:DataIndex + 4])[0]
            DataIndex += 4
            if (Number < 0 and SourceCodeLine[len(SourceCodeLine) - 1] == '+'):
                SourceCodeLine.pop()
            SourceCodeLine.append('%d' % Number)
        elif cmd == 0x10:
            Number = struct.unpack('<Q', InputData[DataIndex:DataIndex + 8])[0]
            DataIndex += 8
            SourceCodeLine.append('%d' % Number)
        elif cmd == 0x20:
            Number = struct.unpack('<d', InputData[DataIndex:DataIndex + 8])[0]
            DataIndex += 8
            SourceCodeLine.append('%.13f' % Number)
        elif cmd == 0x30:
            # Keyword
            Size = struct.unpack('<L', InputData[DataIndex:DataIndex + 4])[0]
            DataIndex += 4
            RawString = InputData[DataIndex:DataIndex + (Size * 2)]
            DataIndex += Size * 2
            String = EncDecString(Size, RawString).decode('utf-16').encode('utf-8')
            SourceCodeLine.append(' ' + String + ' ')
        elif cmd == 0x31:
            # BuiltIn
            Size = struct.unpack('<L', InputData[DataIndex:DataIndex + 4])[0]
            DataIndex += 4
            RawString = InputData[DataIndex:DataIndex + (Size * 2)]
            DataIndex += Size * 2
            String = EncDecString(Size, RawString).decode('utf-16').encode('utf-8')
            SourceCodeLine.append(' ' + String + ' ')
        elif cmd == 0x32:
            # Macro
            Size = struct.unpack('<L', InputData[DataIndex:DataIndex + 4])[0]
            DataIndex += 4
            RawString = InputData[DataIndex:DataIndex + (Size * 2)]
            DataIndex += Size * 2
            String = EncDecString(Size, RawString).decode('utf-16').encode('utf-8')
            SourceCodeLine.append(' @' + String + ' ')
        elif cmd == 0x33:
            # Variable
            Size = struct.unpack('<L', InputData[DataIndex:DataIndex + 4])[0]
            DataIndex += 4
            RawString = InputData[DataIndex:DataIndex + (Size * 2)]
            DataIndex += Size * 2
            String = EncDecString(Size, RawString).decode('utf-16').encode('utf-8')
            SourceCodeLine.append(' $' + String + ' ')
        elif cmd == 0x34:
            # FunctionCall
            Size = struct.unpack('<L', InputData[DataIndex:DataIndex + 4])[0]
            DataIndex += 4
            RawString = InputData[DataIndex:DataIndex + (Size * 2)]
            DataIndex += Size * 2
            String = EncDecString(Size, RawString).decode('utf-16').encode('utf-8')
            SourceCodeLine.append(' ' + String + ' ')
        elif cmd == 0x36:
            # UserString
            Size = struct.unpack('<L', InputData[DataIndex:DataIndex + 4])[0]
            DataIndex += 4
            RawString = InputData[DataIndex:DataIndex + (Size * 2)]
            DataIndex += Size * 2
            String = EncDecString(Size, RawString).decode('utf-16').encode('utf-8')
            SourceCodeLine.append(' "' + String + '" ')
        elif cmd == 0x40:
            SourceCodeLine.append(',')
        elif cmd == 0x41:
            SourceCodeLine.append('=')
        elif cmd == 0x42:
            SourceCodeLine.append('>')
        elif cmd == 0x43:
            SourceCodeLine.append('<')
        elif cmd == 0x44:
            SourceCodeLine.append('<>')
        elif cmd == 0x45:
            SourceCodeLine.append('>=')
        elif cmd == 0x46:
            SourceCodeLine.append('<=')
        elif cmd == 0x47:
            SourceCodeLine.append('(')
        elif cmd == 0x48:
            SourceCodeLine.append(')')
        elif cmd == 0x49:
            SourceCodeLine.append('+')
        elif cmd == 0x4A:
            SourceCodeLine.append('-')
        elif cmd == 0x4B:
            SourceCodeLine.append('/')
        elif cmd == 0x4C:
            SourceCodeLine.append('*')
        elif cmd == 0x4D:
            SourceCodeLine.append('&')
        elif cmd == 0x4E:
            SourceCodeLine.append('[')
        elif cmd == 0x4F:
            SourceCodeLine.append(']')
        elif cmd == 0x50:
            SourceCodeLine.append('==')
        elif cmd == 0x51:
            SourceCodeLine.append('^')
        elif cmd == 0x52:
            SourceCodeLine.append('+=')
        elif cmd == 0x53:
            SourceCodeLine.append('-=')
        elif cmd == 0x54:
            SourceCodeLine.append('/=')
        elif cmd == 0x55:
            SourceCodeLine.append('*=')
        elif cmd == 0x56:
            SourceCodeLine.append('&=')
        elif cmd == 0x57:
            SourceCodeLine.append('?')
        elif cmd == 0x58:
            SourceCodeLine.append(':')
        elif cmd == 0x7F:
            ScriptText += '%s\r\n' % ''.join(SourceCodeLine)
            SourceCodeLine = []
            Line = Line + 1
        else:
            print 'Unknown cmd token: %02X' % cmd
    return ScriptText

if __name__ == '__main__':
    ScriptData = open(sys.argv[1], 'rb').read()
    ScriptText = DataToText(ScriptData)

    ScriptFile = open(sys.argv[2], 'wb')
    ScriptFile.write(ScriptText)
    ScriptFile.close()
