#!/usr/bin/var python2

import sys, struct, binascii
import Tokeniser, DeTokeniser

class AU3Variable:
    def __init__(self, Name, StartOffset, Length, Used):
        self.Name = Name
        self.StartOffset = StartOffset
        self.Length = Length
        self.Used = Used

def RemoveDeadVars(TokensList):
    # Traverse the list to find all the variables at the beginning of the line
    # We are keeping it simple right now, don't consider Global/Local ones
    # Also check if we are dealing with a plain ('=') assigmenent and if yes
    # if what's being assigned is not the return value of a function (built-in or user defined).
    # If all the requirements are fulfilled create an instance of an AU3Variable class
    # with the Name, Index in the TokensList, length of the var (from name to end of line)
    # and add it to the VarsList
    Index = 0
    VarsList = []
    VarsRemovedNum = 0
    while Index < len(TokensList):
        if TokensList[Index][0] == 0x33 and (Index == 0 or TokensList[Index - 1][0] == 0x7F):
            if TokensList[Index + 1][0] == 0x41 and TokensList[Index + 2][0] not in {0x31, 0x34}:
                Name = TokensList[Index][2]
                StartOffset = Index
                Index += 1
                while TokensList[Index][0] != 0x7F:
                    Index += 1
                Length = (Index - StartOffset) + 1
                VarsList.append(AU3Variable(Name, StartOffset, Length, False))
        Index += 1

    # For every Var found traverse TokensList. Every time we meet a variable there
    # check if it is at the beginning of a line and if it is check if the name of this
    # variable is equal to the one we are examining from the VarsList.
    # If it is not at the beginning of a line mark it as used.
    for Variable in VarsList:
        Index = 0
        while Index < len(TokensList):
            if TokensList[Index][0] == 0x33 and Index != 0 and TokensList[Index - 1][0] != 0x7F and Variable.Name == TokensList[Index][2]:
                # print 'Setting %s as used' % Variable.Name
                Variable.Used = True
                break
            Index += 1

    # And then I'm tired as fuck and today I decide I'm done commenting my code
    Offset = 0
    for Variable in VarsList:
        if Variable.Used == False:
            for NumPops in xrange(Variable.Length):
                TokensList.pop(Variable.StartOffset - Offset)
            Offset += Variable.Length
            VarsRemovedNum += 1
    return VarsRemovedNum, TokensList

if __name__ == '__main__':
    ScriptText = open(sys.argv[1], 'rb').read() + '\r\n'
    TokensList = Tokeniser.TextToTokensList(ScriptText, len(ScriptText))

    # for Elem in TokensList:
    #     print Elem

    while 1:
        VarsRemovedNum, TokensList = RemoveDeadVars(TokensList)
        print 'Removed %s lines' % VarsRemovedNum
        if VarsRemovedNum == 0:
            break

    # for Elem in TokensList:
    #     print Elem

    TokenData = Tokeniser.TokensListToData(TokensList)
    ScriptText = DeTokeniser.DataToText(TokenData)

    ScriptFile = open(sys.argv[2], 'wb')
    ScriptFile.write(ScriptText)
    ScriptFile.close()
